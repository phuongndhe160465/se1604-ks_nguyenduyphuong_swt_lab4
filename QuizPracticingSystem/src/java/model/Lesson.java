package model;

import dal.LessonDAO;

public class Lesson {
    private int id;
    private int subjectId;
    private float order;
    private int typeId;
    private boolean status;
    private int subjectTopicId;
    private int lessonContentId;
    private int quizId;
    private String name;
    public Lesson() {
    }

    public Lesson(int id, int subjectId, float order, int typeId, boolean status, int subjectTopicId, int lessonContentId, int quizId, String name) {
        this.id = id;
        this.subjectId = subjectId;
        this.order = order;
        this.typeId = typeId;
        this.status = status;
        this.subjectTopicId = subjectTopicId;
        this.lessonContentId = lessonContentId;
        this.quizId = quizId;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public float getOrder() {
        return order;
    }

    public void setOrder(float order) {
        this.order = order;
    }

    public int getTypeId() {
        return typeId;
    }
    
    public LessonType getType(){
        LessonDAO ld = new LessonDAO();
        return ld.getLessonTypeById(typeId);
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getSubjectTopicId() {
        return subjectTopicId;
    }
    
    public SubjectTopic getSubjectTopic(){
        LessonDAO ld = new LessonDAO();
        return ld.getSubjectTopicById(subjectTopicId);
    }

    public void setSubjectTopicId(int subjectTopicId) {
        this.subjectTopicId = subjectTopicId;
    }

    public int getLessonContentId() {
        return lessonContentId;
    }
    
    public void setLessonContentId(int lessonContentId) {
        this.lessonContentId = lessonContentId;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public boolean isLessonSubjectTopicUsed(){
        LessonDAO ld = new LessonDAO();
        return typeId==1&&ld.isSubjectTopicUsed(subjectTopicId);
    }
}
