
package model;

/**
 *
 * @author ACER
 */
public class LessonContent {
    private int id;
    private String htmlContent;

    public LessonContent() {
    }

    public LessonContent(int id, String htmlContent) {
        this.id = id;
        this.htmlContent = htmlContent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }
    
}
