package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Lesson;
import model.LessonContent;
import model.LessonType;
import model.SubjectTopic;

public class LessonDAO extends DBContext {
    public Lesson getLesson(PreparedStatement st) {
        try {
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int id = rs.getInt("id");
                int subjectId = rs.getInt("subjectId");
                float order = rs.getFloat("order");
                int typeId = rs.getInt("typeId");
                boolean status = rs.getBoolean("status");
                int subjectTopicId = rs.getInt("subjectTopicId");
                int lessonContentId = rs.getInt("lessonContentId");
                int quizId = rs.getInt("quizId");
                String name = rs.getString("name");
                return new Lesson(id, subjectId, order, typeId, status, subjectTopicId, lessonContentId, quizId, name);
            }
        } catch (SQLException e) {
        }
        return null;
    }
    
    public ArrayList<Lesson> getLessonsList(PreparedStatement st) {
        ArrayList<Lesson> lessonsList = new ArrayList<>();
        try {
            ResultSet rs = st.executeQuery();
            while(rs.next()) {
                int id = rs.getInt("id");
                int subjectId = rs.getInt("subjectId");
                float order = rs.getFloat("order");
                int typeId = rs.getInt("typeId");
                boolean status = rs.getBoolean("status");
                int subjectTopicId = rs.getInt("subjectTopicId");
                int lessonContentId = rs.getInt("lessonContentId");
                int quizId = rs.getInt("quizId");
                String name = rs.getString("name");
                lessonsList.add(new Lesson(id, subjectId, order, typeId, status, subjectTopicId, lessonContentId, quizId, name));
            }
            return lessonsList;
        } catch (SQLException e) {
        }
        return null;
    }
    
    public Lesson getLessonById(int id) {
        String sql = "select * from Lesson where id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            return getLesson(st);
        } catch (SQLException e) {
        }
        return null;
    }
    
//    public Lesson getLessonByQuestionId(int id) {
//        String sql = "select top 1 l.* from Lesson l join Lesson_Question lq on l.id = lq.lessonId  where lq.questionId=?";
//        try {
//            PreparedStatement st = connection.prepareStatement(sql);
//            st.setInt(1, id);
//            return getLesson(st);
//        } catch (SQLException e) {
//        }
//        return null;
//    }
    
    public LessonType getLessonTypeById(int id) {
        String sql = "select * from Lesson_Type where id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                String name = rs.getString("name");
                String column = rs.getString("column");
                return new LessonType(id, name, column);
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public ArrayList<Lesson> getLessonsFromSubject(String subject) {
        ArrayList<Lesson> lessons = new ArrayList<>();
        try {
            String sql = " select l.* from Lesson l join [Subject] s on  l.subjectId = s.id join Subject_Topic st on l.subjectTopicId = st.id and typeId = 1\n"
                    + "  where s.title = ? ";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, subject);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Lesson l = new Lesson();
                l.setId(rs.getInt("id"));
                l.setSubjectId(rs.getInt("subjectId"));
                l.setOrder(rs.getFloat("order"));
                l.setTypeId(rs.getInt("typeId"));
                l.setStatus(rs.getBoolean("status"));
                l.setSubjectTopicId(rs.getInt("subjectTopicId"));
                l.setLessonContentId(rs.getInt("lessonContentId"));
                l.setQuizId(rs.getInt("quizId"));                
                l.setName(rs.getString("name"));
                lessons.add(l);
            }
        } catch (SQLException ex) {
            Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lessons;
    }
    
    public ArrayList<LessonType> getLessonTypesList() {
        String sql = "select * from Lesson_Type";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            ArrayList<LessonType> typesList = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String column = rs.getString("column");
                typesList.add(new LessonType(id, name, column));
            }
            return typesList;
        } catch (SQLException e) {
        }
        return null;
    }
    
    public ArrayList<SubjectTopic> getSubjectTopicsListBySubjectId(int subjectId) {
        String sql = "select st.* from Subject_Topic st inner join Lesson l on "
                + "st.id = l.subjectTopicId where typeId=1 and subjectId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subjectId);
            ResultSet rs = st.executeQuery();
            ArrayList<SubjectTopic> topicsList = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                topicsList.add(new SubjectTopic(id, name));
            }
            return topicsList;
        } catch (SQLException e) {
        }
        return null;
    }
    
    public boolean isSubjectTopicUsed(int subjectTopicId) {
        String sql = "select count(id) as count from Lesson where subjectTopicId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subjectTopicId);
            ResultSet rs = st.executeQuery();
            if (rs.next() && rs.getInt("count") > 1) {
                return true;
            }
        } catch (SQLException e) {
        }
        return false;
    }
    
    public int insertLessonSubjectTopic(Lesson lesson) {
        try {
            connection.setAutoCommit(false);
            int subjectTopicId = insertSubjectTopic(lesson.getName());
            if (subjectTopicId == 0) {
                connection.rollback();
                return 0;
            }
            String sql = "insert into Lesson (subjectId, [order], typeId, [status],"
                    + " subjectTopicId, name) values (?,?,?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            st.setInt(1, lesson.getSubjectId());
            st.setFloat(2, lesson.getOrder());
            st.setInt(3, lesson.getTypeId());
            st.setBoolean(4, lesson.isStatus());
            st.setInt(5, subjectTopicId);
            st.setString(6, lesson.getName());
            st.executeUpdate();
            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                connection.commit();
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;
    }
    
    public int insertLesson(Lesson lesson, String targetColumn, int targetId) {
        String sql = "insert into Lesson (subjectId, [order], typeId, [status], subjectTopicId, "
                + targetColumn + ", name) values (?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            st.setInt(1, lesson.getSubjectId());
            st.setFloat(2, lesson.getOrder());
            st.setInt(3, lesson.getTypeId());
            st.setBoolean(4, lesson.isStatus());
            st.setInt(5, lesson.getSubjectTopicId());
            st.setInt(6, targetId);
            st.setString(7, lesson.getName());
            st.executeUpdate();
            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;
    }
    
    public SubjectTopic getSubjectTopicById(int topicId) {
        String sql = "select * from Subject_Topic where id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, topicId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                String name = rs.getString("name");
                return new SubjectTopic(topicId, name);
            }
        } catch (SQLException e) {
        }
        return null;
    }
    
    public int insertSubjectTopic(String name) {
        String sql = "insert into Subject_Topic values (?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, name);
            st.executeUpdate();
            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;
    }
    
    public int updateSubjectTopic(SubjectTopic subjectTopic) {
        String sql = "update Subject_Topic set name = ? where id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, subjectTopic.getName());
            st.setInt(2, subjectTopic.getId());
            return st.executeUpdate();
        } catch (SQLException e) {
        }
        return 0;
    }
    
    public int deleteSubjectTopicById(int subjectTopicId) {
        String sql = "delete from Subject_Topic where id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subjectTopicId);
            return st.executeUpdate();
        } catch (SQLException e) {
        }
        return 0;
    }
    
    public LessonContent getLessonContentById(int lessonContentId) {
        String sql = "select * from Lesson_Content where id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, lessonContentId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                String htmlContent = rs.getString("htmlContent");
                return new LessonContent(lessonContentId, htmlContent);
            }
        } catch (SQLException e) {
        }
        return null;
    }
    
    public int insertLessonContent(LessonContent lc) {
        String sql = "insert into Lesson_Content values (?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, lc.getHtmlContent());
            st.executeUpdate();
            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;
    }
    
    public int updateLessonContent(LessonContent lessonContent) {
        String sql = "update Lesson_Content set htmlContent = ? where id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, lessonContent.getHtmlContent());
            st.setInt(2, lessonContent.getId());
            return st.executeUpdate();
        } catch (SQLException e) {
        }
        return 0;
    }
    
    public int deleteLessonContentById(int lessonContentId) {
        String sql = "delete from Lesson_Content where id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, lessonContentId);
            return st.executeUpdate();
        } catch (SQLException e) {
        }
        return 0;
    }

    public int updateLessonToSubjectTopic(Lesson newLesson) {
        Lesson oldLesson = getLessonById(newLesson.getId());
        if(oldLesson==null) return 0;
        int oldTypeId = oldLesson.getTypeId();
        try {
            connection.setAutoCommit(false);
            //if the type isn't changed
            if (oldTypeId == newLesson.getTypeId()) {
                //update the subject topic table
                if (updateSubjectTopic(new SubjectTopic(oldLesson.getSubjectTopicId(), newLesson.getName())) == 0) {
                    connection.rollback();
                    return 0;
                }
                //update the lesson table
                String sql = "update Lesson set [order] = ?, [status] = ?, name = ? where id = ?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setFloat(1, newLesson.getOrder());
                st.setBoolean(2, newLesson.isStatus());
                st.setString(3, newLesson.getName());
                st.setInt(4, newLesson.getId());
                int success = st.executeUpdate();
                if (success > 0) {
                    connection.commit();
                    return success;
                } else {
                    connection.rollback();
                    return 0;
                }
            } else {  //if the type is changed
                //insert into subject topic table
                int insertedSubjectTopicId = insertSubjectTopic(newLesson.getName());
                if (insertedSubjectTopicId == 0) {
                    connection.rollback();
                    return 0;
                }
                //update lesson table and delete the old column id
                String deletedColumn = getLessonTypeById(oldTypeId).getColumn();
                String sql = "update Lesson set [order] = ?, typeId = ?, [status] = ?, subjectTopicId = ?, "
                        + deletedColumn + " = NULL, name = ? where id = ?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setFloat(1, newLesson.getOrder());
                st.setInt(2, newLesson.getTypeId());
                st.setBoolean(3, newLesson.isStatus());
                st.setInt(4, insertedSubjectTopicId);
                st.setString(5, newLesson.getName());
                st.setInt(6, newLesson.getId());
                int success = st.executeUpdate();
                if (success > 0) {
                    //if lesson type was lesson content before, delete the old lesson content
                    if (oldTypeId == 2 && deleteLessonContentById(oldLesson.getLessonContentId()) == 0) {
                        connection.rollback();
                        return 0;
                    }
                    connection.commit();
                    return success;
                } else {
                    connection.rollback();
                    return 0;
                }
            }
        } catch (SQLException e) {
        }
        return 0;
    }
    
    public int updateLessonToLessonContent(Lesson newLesson, LessonContent newLessonContent) {
        Lesson oldLesson = getLessonById(newLesson.getId());
        if(oldLesson==null) return 0;
        int oldTypeId = oldLesson.getTypeId();
        int newTypeId = 2;
        try {
            connection.setAutoCommit(false);
            //if the type isn't changed
            if (oldTypeId == newTypeId) {
                //update the lesson content table
                newLessonContent.setId(oldLesson.getLessonContentId());
                if (updateLessonContent(newLessonContent) == 0) {
                    connection.rollback();
                    return 0;
                }
                //update the lesson table
                String sql = "update Lesson set [order] = ?, [status] = ?, "
                        + "subjectTopicId = ?, name = ? where id = ?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setFloat(1, newLesson.getOrder());
                st.setBoolean(2, newLesson.isStatus());
                st.setInt(3, newLesson.getSubjectTopicId());
                st.setString(4, newLesson.getName());
                st.setInt(5, newLesson.getId());
                int success = st.executeUpdate();
                if (success > 0) {
                    connection.commit();
                    return success;
                } else {
                    connection.rollback();
                    return 0;
                }
            } else {  //if the type is changed
                //delete old lesson
                String deletedColumn = getLessonTypeById(oldTypeId).getColumn();
                String sql1 = "update Lesson set " + deletedColumn + " = null where id = ?";
                PreparedStatement st1 = connection.prepareStatement(sql1);
                st1.setInt(1, oldLesson.getId());
                st1.executeUpdate();
                if (oldTypeId == 1) {
                    if(deleteSubjectTopicById(oldLesson.getSubjectTopicId())==0){
                        connection.rollback();
                        return 0;
                    }
                } else if (oldTypeId == 2) {
                    if(deleteLessonContentById(oldLesson.getLessonContentId())==0){
                        connection.rollback();
                        return 0;
                    }
                }
                //insert into the lesson content table
                int insertedLessonContentId = insertLessonContent(newLessonContent);
                if (insertedLessonContentId == 0) {
                    connection.rollback();
                    return 0;
                }
                //update the lesson table
                String sql = "update Lesson set [order] = ?, typeId=2, [status] = ?, "
                        + "subjectTopicId = ?, lessonContentId = ?,name = ? where id = ?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setFloat(1, newLesson.getOrder());
                st.setBoolean(2, newLesson.isStatus());
                st.setInt(3, newLesson.getSubjectTopicId());
                st.setInt(4, insertedLessonContentId);
                st.setString(5, newLesson.getName());
                st.setInt(6, newLesson.getId());
                int success = st.executeUpdate();
                if (success > 0) {
                    connection.commit();
                    return success;
                } else {
                    connection.rollback();
                    return 0;
                }
            }
        } catch (SQLException e) {
        }
        return 0;
    }
    
    public int updateLessonToQuiz(Lesson newLesson, int quizId) {
        Lesson oldLesson = getLessonById(newLesson.getId());
        if(oldLesson==null) return 0;
        int oldTypeId = oldLesson.getTypeId();
        int newTypeId = 3;
        try {
            connection.setAutoCommit(false);
            //if the type isn't changed
            if (oldTypeId == newTypeId) {
                //update lesson table
                String sql = "update Lesson set [order] = ?, [status] = ?, "
                        + "subjectTopicId = ?, quizId = ?, name = ? where id = ?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setFloat(1, newLesson.getOrder());
                st.setBoolean(2, newLesson.isStatus());
                st.setInt(3, newLesson.getSubjectTopicId());
                st.setInt(4, quizId);
                st.setString(5, newLesson.getName());
                st.setInt(6, newLesson.getId());
                int success = st.executeUpdate();
                if (success > 0) {
                    connection.commit();
                    return success;
                } else {
                    connection.rollback();
                    return 0;
                }
            } else {  //if the type is changed
                //delete old lesson
                String deletedColumn = getLessonTypeById(oldTypeId).getColumn();
                String sql1 = "update Lesson set " + deletedColumn + " = null where id = ?";
                PreparedStatement st1 = connection.prepareStatement(sql1);
                st1.setInt(1, oldLesson.getId());
                st1.executeUpdate();
                if (oldTypeId == 1) {
                    if(deleteSubjectTopicById(oldLesson.getSubjectTopicId())==0){
                        connection.rollback();
                        return 0;
                    }
                } else if (oldTypeId == 2) {
                    if(deleteLessonContentById(oldLesson.getLessonContentId())==0){
                        connection.rollback();
                        return 0;
                    }
                }
                //update lesson table
                String sql2 = "update Lesson set [order] = ?, typeId=3, [status] = ?, "
                        + "subjectTopicId = ?, quizId = ?, name = ? where id = ?";
                PreparedStatement st2 = connection.prepareStatement(sql2);
                st2.setFloat(1, newLesson.getOrder());
                st2.setBoolean(2, newLesson.isStatus());
                st2.setInt(3, newLesson.getSubjectTopicId());
                st2.setInt(4, quizId);
                st2.setString(5, newLesson.getName());
                st2.setInt(6, newLesson.getId());
                int success = st2.executeUpdate();
                if (success > 0) {
                    connection.commit();
                    return success;
                } else {
                    connection.rollback();
                    return 0;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public ArrayList<Lesson> getLessonSubjectTopicsBySubjectId(int subjectId) {
        ArrayList<Lesson> lessons = new ArrayList<>();
        try {
            String sql = "  Select l.* from Lesson l join Subject_Topic st on l.subjectTopicId = st.id and typeId = 1\n"
                    + "WHERE l.subjectId = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, subjectId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Lesson l = new Lesson();
                l.setId(rs.getInt("id"));
                l.setSubjectId(subjectId);
                l.setOrder(rs.getFloat("order"));
                l.setTypeId(rs.getInt("typeId"));
                l.setStatus(rs.getBoolean("status"));
                l.setSubjectTopicId(rs.getInt("subjectTopicId"));
                l.setLessonContentId(rs.getInt("lessonContentId"));
                l.setQuizId(rs.getInt("quizId"));                
                l.setName(rs.getString("name"));
                lessons.add(l);
            }
        } catch (SQLException ex) {
            Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lessons;
    }
    
    public int deleteLesson(int lessonId){
        String sql = "delete from Lesson where id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, lessonId);
            return st.executeUpdate();
        } catch (SQLException e) {
        }
        return 0;
    }
    
    public int toggleLessonStatus(int id, boolean status){
        String sql = "update Lesson set status=? where id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, status);
            st.setInt(2, id);
            return st.executeUpdate();
        } catch (SQLException e) {
        }
        return 0;
    }

    public ArrayList<Lesson> getLessonsBySubjectId(int subjectId) {
        String sql = "select * from lesson where subjectid=?";
        ArrayList<Lesson> lessons = new ArrayList<>();
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, subjectId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Lesson l = new Lesson();
                l.setId(rs.getInt("id"));
                l.setSubjectId(subjectId);
                l.setOrder(rs.getFloat("order"));
                l.setTypeId(rs.getInt("typeId"));
                l.setStatus(rs.getBoolean("status"));
                l.setSubjectTopicId(rs.getInt("subjectTopicId"));
                l.setLessonContentId(rs.getInt("lessonContentId"));
                l.setQuizId(rs.getInt("quizId"));                
                l.setName(rs.getString("name"));
                lessons.add(l);
            }
            return lessons;
        } catch (SQLException ex) {
            Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public ArrayList<Lesson> getOrderedLessons1BySubjectId(int subjectId){
        String sql = "select * from Lesson where status=1 and subjectId=?"
                + " and typeId=1 order by [order] asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subjectId);
            return getLessonsList(st);
        } catch (SQLException e) {
        }
        return null;
    }
    
    public ArrayList<Lesson> getOrderedLessons2And3BySubjectTopicId(int subjectTopicId){
        String sql = "select * from Lesson where status=1 and subjectTopicId=?"
                + " and typeId!=1 order by [order] asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subjectTopicId);
            return getLessonsList(st);
        } catch (SQLException e) {
        }
        return null;
    }
    
    public ArrayList<Lesson> getFilteredLessonsListOffset(String condition, String order, int offset, int pageSize){
        String sql = "select * from Lesson where "+condition+" order by "+order+" offset ? rows fetch next ? rows only";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, offset);
            st.setInt(2, pageSize);
            return getLessonsList(st);
        } catch (SQLException e) {
        }
        return null;
    }
    
    public int getLessonsListSize(String condition){
        String sql = "select count(id) from Lesson where "+condition;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if(rs.next()) return rs.getInt(1);
        } catch (SQLException e) {
        }
        return 0;
    }
}
