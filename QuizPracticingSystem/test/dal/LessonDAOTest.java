package dal;

import java.util.ArrayList;
import model.Lesson;
import model.LessonContent;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author BirdEye Chili
 */
public class LessonDAOTest {
    
    public LessonDAOTest() {
        //uncomment what you want to test, one at a time please
    }
    
//            @Test
//            public void testGetLessonTypeById() {
//                LessonDAO ld = new LessonDAO();
//                assertTrue(ld.getLessonTypeById(1)!=null);
//                assertTrue(ld.getLessonTypeById(2)!=null);
//                assertTrue(ld.getLessonTypeById(3)!=null);
//                assertTrue(ld.getLessonTypeById(0)==null);
//                assertTrue(ld.getLessonTypeById(10)==null);
//            }
//
//            @Test
//            public void testGetLessonsFromSubject() {
//                LessonDAO ld = new LessonDAO();
//                assertTrue(ld.getLessonsFromSubject("Statistic and Probability").size()>0);
//                assertTrue(ld.getLessonsFromSubject("Computer Architecture and Organization").size()>0);
//                assertTrue(ld.getLessonsFromSubject("abcxyz").size()==0);
//                assertTrue(ld.getLessonsFromSubject("test").size()==0);
//            }
//
//            @Test
//            public void testInsertLessonSubjectTopic() {
//                Lesson lesson1 = new Lesson(0, 1, 12, 1, true, 4, 0, 0, "test1");
//                Lesson lesson2 = new Lesson(0, 2, 12, 1, true, 9, 0, 0, "test2");
//                Lesson lesson3 = new Lesson(0, 1, 12, 1, true, 10, 0, 0, "test3");
//                Lesson lesson4 = new Lesson(0, 3, 12, 1, true, 10, 0, 0, "test3");
//                LessonDAO ld = new LessonDAO();
//                assertTrue(ld.insertLessonSubjectTopic(lesson1)>0);
//                assertTrue(ld.insertLessonSubjectTopic(lesson2)>0);
//                assertTrue(ld.insertLessonSubjectTopic(lesson3)>0);
//                assertTrue(ld.insertLessonSubjectTopic(lesson4)==0);
//            }
//
//            
//            @Test
//            public void testGetLessonById() {
//                LessonDAO ld = new LessonDAO();
//                assertTrue(ld.getLessonById(18)!=null);
//                assertTrue(ld.getLessonById(20)!=null);
//                assertTrue(ld.getLessonById(21)!=null);
//                assertTrue(ld.getLessonById(22)!=null);
//                assertTrue(ld.getLessonById(26)!=null);
//                assertTrue(ld.getLessonById(27)!=null);
//                assertTrue(ld.getLessonById(30)!=null);
//                assertTrue(ld.getLessonById(31)!=null);
//                assertTrue(ld.getLessonById(32)!=null);
//                assertTrue(ld.getLessonById(33)!=null);
//                assertTrue(ld.getLessonById(0)==null);
//                assertTrue(ld.getLessonById(50)==null);
//                assertTrue(ld.getLessonById(100)==null);
//            }
//
//            @Test
//            public void testGetSubjectTopicsListBySubjectId() {
//                LessonDAO ld = new LessonDAO();
//                assertTrue(ld.getSubjectTopicsListBySubjectId(1).size()>0);
//                assertTrue(ld.getSubjectTopicsListBySubjectId(2).size()>0);
//                assertTrue(ld.getSubjectTopicsListBySubjectId(0).size()==0);
//                assertTrue(ld.getSubjectTopicsListBySubjectId(100).size()==0);
//            }
//
//
//            @Test
//            public void testInsertLesson() {
//                Lesson lesson = new Lesson(0, 1, 12, 3, true, 4, 0, 0, "test");
//                Lesson lesson2 = new Lesson(0, 1, 12, 3, true, 0, 0, 0, "test");
//                LessonDAO ld = new LessonDAO();
//                assertTrue(ld.insertLesson(lesson, "quizId", 9)>0);
//                assertTrue(ld.insertLesson(lesson, "quizId", 1)>0);
//                assertTrue(ld.insertLesson(lesson, "testId", 1)==0);
//                assertTrue(ld.insertLesson(lesson2, "quizId", 1)==0);
//                assertTrue(ld.insertLesson(lesson, "quizId", 0)==0);
//                assertTrue(ld.insertLesson(lesson, "quizId", 100)==0);
//            }
//
//            @Test
//            public void testGetSubjectTopicById() {
//                LessonDAO ld = new LessonDAO();
//                assertTrue(ld.getSubjectTopicById(4)!=null);
//                assertTrue(ld.getSubjectTopicById(9)!=null);
//                assertTrue(ld.getSubjectTopicById(0)==null);
//                assertTrue(ld.getSubjectTopicById(100)==null);
//            }
//
//            @Test
//            public void testUpdateLessonToSubjectTopic() {
//                Lesson newLesson1 = new Lesson(18, 1, 2, 1, true, 0, 0, 0, "testupdateltst");
//                Lesson newLesson2 = new Lesson(20, 1, 300, 1, false, 0, 0, 0, "test");
//                Lesson newLesson3 = new Lesson(21, 1, 2, 1, true, 0, 0, 0, "testupdateltst");
//                Lesson newLesson4 = new Lesson(27, 1, 300, 1, false, 0, 0, 0, "test");
//                Lesson newLesson5 = new Lesson(0, 1, 300, 1, false, 0, 0, 0, "test");
//                Lesson newLesson6 = new Lesson(100, 1, 300, 1, false, 0, 0, 0, "test");
//                Lesson newLesson7 = new Lesson(20, 1, 300, 4, false, 0, 0, 0, "test");
//                LessonDAO ld = new LessonDAO();
//                assertEquals(1, ld.updateLessonToSubjectTopic(newLesson1));
//                assertEquals(1, ld.updateLessonToSubjectTopic(newLesson2));
//                assertEquals(1, ld.updateLessonToSubjectTopic(newLesson3));
//                assertEquals(1, ld.updateLessonToSubjectTopic(newLesson4));
//                assertEquals(0, ld.updateLessonToSubjectTopic(newLesson5));
//                assertEquals(0, ld.updateLessonToSubjectTopic(newLesson6));
//                assertEquals(0, ld.updateLessonToSubjectTopic(newLesson7));
//            }
//
//            @Test
//            public void testUpdateLessonToLessonContent() {
//                Lesson newLesson1 = new Lesson(31, 0, 1, 2, false, 4, 4, 0, "namelc");
//                Lesson newLesson2 = new Lesson(32, 0, 1, 2, false, 4, 5, 0, "namelc");
//                Lesson newLesson3 = new Lesson(33, 0, 1, 2, false, 4, 0, 0, "namelc");
//                Lesson newLesson4 = new Lesson(22, 0, 1, 2, false, 4, 0, 0, "namelc");
//                Lesson newLesson5 = new Lesson(0, 0, 1, 2, false, 4, 0, 0, "namelc");
//                Lesson newLesson6 = new Lesson(100, 0, 1, 2, false, 4, 0, 0, "namelc");
//                Lesson newLesson7 = new Lesson(18, 0, 1, 2, false, 4, 0, 0, "namelc");
//                LessonContent newLessonContent = new LessonContent(0, "testupdateltlc");
//                LessonDAO ld = new LessonDAO();
//                assertEquals(1, ld.updateLessonToLessonContent(newLesson1, newLessonContent));
//                assertEquals(1, ld.updateLessonToLessonContent(newLesson2, newLessonContent));
//                assertEquals(1, ld.updateLessonToLessonContent(newLesson3, newLessonContent));
//                assertEquals(1, ld.updateLessonToLessonContent(newLesson4, newLessonContent));
//                assertEquals(0, ld.updateLessonToLessonContent(newLesson5, newLessonContent));
//                assertEquals(0, ld.updateLessonToLessonContent(newLesson6, newLessonContent));
//                assertEquals(0, ld.updateLessonToLessonContent(newLesson7, newLessonContent));
//            }
//
//            @Test
//            public void testUpdateLessonToQuiz() {
//                Lesson newLesson1 = new Lesson(32, 1, 1, 0, true, 4, 0, 0, "testupdateltq");
//                Lesson newLesson2 = new Lesson(33, 1, 1, 0, true, 4, 0, 0, "testupdateltq");
//                Lesson newLesson3 = new Lesson(30, 1, 1, 0, true, 4, 0, 0, "testupdateltq");
//                Lesson newLesson4 = new Lesson(0, 1, 1, 0, true, 4, 0, 0, "testupdateltq");
//                Lesson newLesson5 = new Lesson(100, 1, 1, 0, true, 4, 0, 0, "testupdateltq");
//                Lesson newLesson6 = new Lesson(50, 1, 1, 0, true, 4, 0, 0, "testupdateltq");
//                Lesson newLesson7 = new Lesson(18, 1, 1, 0, true, 4, 0, 0, "testupdateltq");
//                LessonDAO ld = new LessonDAO();
//                assertEquals(1, ld.updateLessonToQuiz(newLesson1, 1));
//                assertEquals(1, ld.updateLessonToQuiz(newLesson2, 1));
//                assertEquals(0, ld.updateLessonToQuiz(newLesson3, 1));
//                assertEquals(0, ld.updateLessonToQuiz(newLesson4, 1));
//                assertEquals(0, ld.updateLessonToQuiz(newLesson5, 1));
//                assertEquals(0, ld.updateLessonToQuiz(newLesson6, 1));
//                assertEquals(0, ld.updateLessonToQuiz(newLesson7, 1));
//            }
//
//            @Test
//            public void testGetLessonSubjectTopicsBySubjectId() {
//                LessonDAO ld = new LessonDAO();
//                assertTrue(ld.getLessonSubjectTopicsBySubjectId(1).size()>0);
//                assertTrue(ld.getLessonSubjectTopicsBySubjectId(2).size()>0);
//                assertTrue(ld.getLessonSubjectTopicsBySubjectId(0).size()==0);
//                assertTrue(ld.getLessonSubjectTopicsBySubjectId(10).size()==0);
//            }
//
//
//            @Test
//            public void testGetLessonsBySubjectId() {
//                LessonDAO ld = new LessonDAO();
//                assertTrue(ld.getLessonsBySubjectId(1).size()>0);
//                assertTrue(ld.getLessonsBySubjectId(2).size()>0);
//                assertTrue(ld.getLessonsBySubjectId(0).size()==0);
//                assertTrue(ld.getLessonsBySubjectId(10).size()==0);
//            }
//
//            @Test
//            public void testGetOrderedLessons1BySubjectId() {
//                LessonDAO ld = new LessonDAO();
//                assertTrue(ld.getOrderedLessons1BySubjectId(1).size()>0);
//                assertTrue(ld.getOrderedLessons1BySubjectId(2).size()>0);
//                assertTrue(ld.getOrderedLessons1BySubjectId(0).size()==0);
//                assertTrue(ld.getOrderedLessons1BySubjectId(10).size()==0);
//            }
//
//            @Test
//            public void testGetOrderedLessons2And3BySubjectTopicId() {
//                LessonDAO ld = new LessonDAO();
//                assertTrue(ld.getOrderedLessons2And3BySubjectTopicId(4).size()>0);
//                assertTrue(ld.getOrderedLessons2And3BySubjectTopicId(12).size()>0);
//                assertTrue(ld.getOrderedLessons2And3BySubjectTopicId(0).size()==0);
//                assertTrue(ld.getOrderedLessons2And3BySubjectTopicId(9).size()==0);
//            }
//
//            @Test
//            public void testGetFilteredLessonsListOffset() {
//                LessonDAO ld = new LessonDAO();
//                ArrayList<Lesson> result1 = ld.getFilteredLessonsListOffset("id>1", "id desc", 0, 10);
//                ArrayList<Lesson> result2 = ld.getFilteredLessonsListOffset("id=0", "id desc", 0, 10);
//                ArrayList<Lesson> result3 = ld.getFilteredLessonsListOffset("id>1", "test", 0, 10);
//                ArrayList<Lesson> result4 = ld.getFilteredLessonsListOffset("id>1", "id desc", 100, 10);
//                ArrayList<Lesson> result5 = ld.getFilteredLessonsListOffset("id>1", "id desc", 0, 0);
//                assertTrue(result1.size()>0);
//                assertTrue(result2.size()==0);
//                assertTrue(result3==null);
//                assertTrue(result4.size()==0);
//                assertTrue(result5==null);
//            }
//    
//            @Test
//            public void testInsertSubjectTopic() {
//                LessonDAO ld = new LessonDAO();
//                assertTrue(ld.insertSubjectTopic("test")>0);
//                assertTrue(ld.insertSubjectTopic("")>0);
//            }
}
