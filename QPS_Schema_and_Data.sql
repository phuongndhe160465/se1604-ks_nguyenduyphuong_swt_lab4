USE [master]
GO
/****** Object:  Database [QuizPracticingSystem]    Script Date: 7/17/2022 3:17:20 PM ******/
CREATE DATABASE [QuizPracticingSystem]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QuizPracticingSystem', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\QuizPracticingSystem.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QuizPracticingSystem_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\QuizPracticingSystem_log.ldf' , SIZE = 1088KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QuizPracticingSystem] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QuizPracticingSystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QuizPracticingSystem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET ARITHABORT OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [QuizPracticingSystem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QuizPracticingSystem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QuizPracticingSystem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET  ENABLE_BROKER 
GO
ALTER DATABASE [QuizPracticingSystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QuizPracticingSystem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QuizPracticingSystem] SET  MULTI_USER 
GO
ALTER DATABASE [QuizPracticingSystem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QuizPracticingSystem] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QuizPracticingSystem] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QuizPracticingSystem] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [QuizPracticingSystem] SET DELAYED_DURABILITY = DISABLED 
GO
USE [QuizPracticingSystem]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[email] [varchar](100) NOT NULL,
	[password] [varchar](100) NOT NULL,
	[roleId] [int] NULL,
 CONSTRAINT [PK_Account_1] PRIMARY KEY CLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Account_Function]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account_Function](
	[email] [varchar](100) NOT NULL,
	[functionId] [int] NOT NULL,
 CONSTRAINT [PK_Account_Function_1] PRIMARY KEY CLUSTERED 
(
	[email] ASC,
	[functionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Account_Key]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account_Key](
	[email] [varchar](100) NOT NULL,
	[keyRandom] [varchar](50) NOT NULL,
	[expiredDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Account_Key_1] PRIMARY KEY CLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Answer]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Answer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [varchar](1000) NOT NULL,
	[questionId] [int] NOT NULL,
	[status] [bit] NOT NULL,
 CONSTRAINT [PK_Answer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Answer_History]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answer_History](
	[quizHistoryId] [int] NOT NULL,
	[answerId] [int] NOT NULL,
 CONSTRAINT [PK_Answer_History] PRIMARY KEY CLUSTERED 
(
	[quizHistoryId] ASC,
	[answerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Blog]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Blog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[subCategoryId] [int] NOT NULL,
	[authorId] [int] NOT NULL,
	[title] [varchar](500) NULL,
	[updatedDate] [date] NULL,
	[thumbnail] [varchar](500) NULL,
	[briefInformation] [varchar](500) NULL,
	[status] [bit] NULL,
	[featuring] [varchar](50) NULL,
	[description] [varchar](1000) NULL,
 CONSTRAINT [PK_Blog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Blog_Category]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Blog_Category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
 CONSTRAINT [PK_Blog_Category_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Blog_Sub_Category]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Blog_Sub_Category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[categoryId] [int] NOT NULL,
 CONSTRAINT [PK_Blog_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dimension]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dimension](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[description] [varchar](1000) NULL,
	[typeId] [int] NULL,
	[subjectId] [int] NULL,
 CONSTRAINT [PK_Dimension] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dimension_Type]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dimension_Type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
 CONSTRAINT [PK_Type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Function]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Function](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[url] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Function] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Function_Role]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Function_Role](
	[functionId] [int] NOT NULL,
	[roleId] [int] NOT NULL,
 CONSTRAINT [PK_Function_Role] PRIMARY KEY CLUSTERED 
(
	[functionId] ASC,
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Lesson]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Lesson](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[subjectId] [int] NOT NULL,
	[order] [decimal](5, 2) NOT NULL,
	[typeId] [int] NOT NULL,
	[status] [bit] NOT NULL,
	[subjectTopicId] [int] NULL,
	[lessonContentId] [int] NULL,
	[quizId] [int] NULL,
	[name] [varchar](100) NULL,
 CONSTRAINT [PK_Lesson] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lesson_Content]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Lesson_Content](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[htmlContent] [varchar](1000) NULL,
 CONSTRAINT [PK_Lesson_Content] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lesson_Type]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Lesson_Type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[column] [varchar](100) NULL,
 CONSTRAINT [PK_Lesson_Type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Level]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Level](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
 CONSTRAINT [PK_Level] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Media_Type]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Media_Type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
 CONSTRAINT [PK_Media_Type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[New_Customers]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[New_Customers](
	[register] [int] NULL,
	[bought] [int] NULL,
	[registrationdate] [date] NOT NULL,
 CONSTRAINT [PK_NewCustomers] PRIMARY KEY CLUSTERED 
(
	[registrationdate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Price_Package]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Price_Package](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[duration] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[listPrice] [decimal](10, 2) NOT NULL,
	[salePrice] [decimal](10, 2) NOT NULL,
	[subjectId] [int] NOT NULL,
	[status] [bit] NULL,
	[description] [varchar](500) NULL,
 CONSTRAINT [PK_PricePackage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Price_Package_Lesson]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Price_Package_Lesson](
	[pricePackageId] [int] NOT NULL,
	[lessonId] [int] NOT NULL,
 CONSTRAINT [PK_PricePackage_Lesson] PRIMARY KEY CLUSTERED 
(
	[pricePackageId] ASC,
	[lessonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Question]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Question](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [varchar](1000) NOT NULL,
	[subjectId] [int] NOT NULL,
	[levelId] [int] NOT NULL,
	[status] [bit] NOT NULL,
	[media] [varchar](1000) NULL,
	[mediaTypeId] [int] NULL,
	[dimensionId] [int] NULL,
	[lessonId] [int] NULL,
	[explanation] [varchar](1000) NULL,
 CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Question_History]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question_History](
	[quizHistoryId] [int] NOT NULL,
	[questionId] [int] NOT NULL,
	[markStatus] [bit] NULL,
 CONSTRAINT [PK_Question_History] PRIMARY KEY CLUSTERED 
(
	[quizHistoryId] ASC,
	[questionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Question_Quiz]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question_Quiz](
	[quizId] [int] NOT NULL,
	[questionId] [int] NOT NULL,
 CONSTRAINT [PK_Question_Quiz] PRIMARY KEY CLUSTERED 
(
	[quizId] ASC,
	[questionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Quiz]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quiz](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[subjectId] [int] NOT NULL,
	[levelId] [int] NULL,
	[duration] [int] NULL,
	[passRate] [decimal](3, 0) NULL,
	[typeId] [int] NOT NULL,
	[description] [varchar](1000) NULL,
 CONSTRAINT [PK_Quiz] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Quiz_History]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quiz_History](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[quizId] [int] NOT NULL,
	[userId] [int] NOT NULL,
	[timeStarted] [datetime] NULL,
	[timeFinished] [datetime] NULL,
	[subjectId] [int] NOT NULL,
 CONSTRAINT [PK_Quiz_History] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Quiz_Type]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quiz_Type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
 CONSTRAINT [PK_Quiz_Type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Registration]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Registration](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[subjectId] [int] NOT NULL,
	[email] [varchar](100) NULL,
	[packageId] [int] NOT NULL,
	[registrationTime] [datetime] NULL,
	[totalCost] [decimal](10, 2) NULL,
	[status] [bit] NULL,
	[validFrom] [date] NULL,
	[lastUpdatedBy] [int] NULL,
	[note] [nvarchar](200) NULL,
	[sale] [float] NULL,
	[validTo] [date] NULL,
 CONSTRAINT [PK_Registration] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Registrations_By_Day]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Registrations_By_Day](
	[registrationdate] [date] NOT NULL,
	[paid] [int] NULL,
	[total] [int] NULL,
 CONSTRAINT [PK_Registrations_By_Day] PRIMARY KEY CLUSTERED 
(
	[registrationdate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Slider]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Slider](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](100) NULL,
	[thumbnail] [varchar](100) NULL,
	[backlink] [varchar](100) NULL,
	[status] [bit] NULL,
	[note] [varchar](500) NULL,
 CONSTRAINT [PK_Slider] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subject]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subject](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](500) NOT NULL,
	[tagLine] [varchar](100) NULL,
	[subCategoryId] [int] NOT NULL,
	[thumbnail] [varchar](500) NULL,
	[description] [varchar](5000) NULL,
	[ownerId] [int] NOT NULL,
	[status] [bit] NOT NULL,
	[updatedDate] [date] NOT NULL,
	[featured] [bit] NOT NULL,
	[createdDate] [date] NOT NULL,
 CONSTRAINT [PK_Subject] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subject_Category]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subject_Category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
 CONSTRAINT [PK_Subject_Category_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subject_Sub_Category]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subject_Sub_Category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[categoryId] [int] NOT NULL,
 CONSTRAINT [PK_Subject_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subject_Topic]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subject_Topic](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
 CONSTRAINT [PK_Subject_Topic] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Temp_Registration]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_Registration](
	[email] [varchar](100) NULL,
	[name] [varchar](100) NULL,
	[gender] [bit] NULL,
	[phone] [varchar](20) NULL,
	[registrationId] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Total_Registration]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Total_Registration](
	[submitted] [int] NULL,
	[cancel] [int] NULL,
	[success] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[gender] [bit] NOT NULL,
	[phone] [varchar](20) NOT NULL,
	[status] [bit] NULL,
	[address] [varchar](200) NULL,
	[avatar] [varchar](1000) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User_Subject]    Script Date: 7/17/2022 3:17:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Subject](
	[subjectId] [int] NOT NULL,
	[userId] [int] NOT NULL,
 CONSTRAINT [PK_User_Subject] PRIMARY KEY CLUSTERED 
(
	[subjectId] ASC,
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Account] ([email], [password], [roleId]) VALUES (N'abc@fpt.edu.vn', N'yFk5gZKSBxcdjNrta36cKA==', 2)
INSERT [dbo].[Account] ([email], [password], [roleId]) VALUES (N'admin', N'yFk5gZKSBxcdjNrta36cKA==', 1)
INSERT [dbo].[Account] ([email], [password], [roleId]) VALUES (N'bachhhhe160470@fpt.edu.vn', N'yFk5gZKSBxcdjNrta36cKA==', 3)
INSERT [dbo].[Account] ([email], [password], [roleId]) VALUES (N'huy69420@fpt.edu.vn', N'yFk5gZKSBxcdjNrta36cKA==', 1)
INSERT [dbo].[Account] ([email], [password], [roleId]) VALUES (N'namtmhe160442@fpt.edu.vn', N'yFk5gZKSBxcdjNrta36cKA==', 5)
INSERT [dbo].[Account] ([email], [password], [roleId]) VALUES (N'phuongndhe160465@fpt.edu.vn', N'yFk5gZKSBxcdjNrta36cKA==', 1)
INSERT [dbo].[Account] ([email], [password], [roleId]) VALUES (N'subaccfordownloadinggames@gmail.com', N'yFk5gZKSBxcdjNrta36cKA==', 4)
INSERT [dbo].[Account] ([email], [password], [roleId]) VALUES (N'test@gmail.com', N'yFk5gZKSBxcdjNrta36cKA==', 2)
INSERT [dbo].[Account] ([email], [password], [roleId]) VALUES (N'testinsert', N'yFk5gZKSBxcdjNrta36cKA==', 2)
INSERT [dbo].[Account] ([email], [password], [roleId]) VALUES (N'thang96420@fpt.edu.vn', N'yFk5gZKSBxcdjNrta36cKA==', 5)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'abc@fpt.edu.vn', 3)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'abc@fpt.edu.vn', 4)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'abc@fpt.edu.vn', 7)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'abc@fpt.edu.vn', 8)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 1)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 2)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 3)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 4)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 5)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 6)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 7)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 8)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 9)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 10)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 11)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 12)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'admin', 13)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'bachhhhe160470@fpt.edu.vn', 5)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'bachhhhe160470@fpt.edu.vn', 6)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'huy69420@fpt.edu.vn', 1)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'huy69420@fpt.edu.vn', 2)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'huy69420@fpt.edu.vn', 5)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'huy69420@fpt.edu.vn', 6)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'huy69420@fpt.edu.vn', 7)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'huy69420@fpt.edu.vn', 8)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'namtmhe160442@fpt.edu.vn', 14)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'namtmhe160442@fpt.edu.vn', 21)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'namtmhe160442@fpt.edu.vn', 22)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'namtmhe160442@fpt.edu.vn', 23)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'namtmhe160442@fpt.edu.vn', 24)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 1)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 2)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 3)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 4)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 5)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 6)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 9)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 10)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 11)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 12)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 13)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 14)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 15)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 16)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 17)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 18)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 19)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 20)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 21)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 22)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 23)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 24)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 25)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 26)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 27)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'phuongndhe160465@fpt.edu.vn', 28)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'thang96420@fpt.edu.vn', 1)
INSERT [dbo].[Account_Function] ([email], [functionId]) VALUES (N'thang96420@fpt.edu.vn', 22)
SET IDENTITY_INSERT [dbo].[Answer] ON 

INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (99, N'a. 0.45', 6, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (100, N'b. 0.18', 6, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (101, N'c. 0.48', 6, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (119, N'A. 1', 3, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (120, N'B. 2', 3, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (126, N'A. 1NF', 7, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (127, N'B. 2NF', 7, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (128, N'C. 3NF', 7, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (135, N'PK', 16, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (136, N'FK', 16, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (142, N'A. yes', 14, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (143, N'B. no', 14, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (147, N'A. print', 17, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (148, N'B. println', 17, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (159, N'test', 20, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (160, N'test', 21, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (164, N'xzcxz', 22, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (165, N'xzcxzc', 22, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (166, N'cxcv', 22, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (169, N'dsadasdsa', 23, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (170, N'asdsadsa', 23, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (175, N'sadsad', 24, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (176, N'sadasdsaasdsad', 24, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (177, N'a. all women custormers', 1, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (178, N'b. all', 1, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (185, N'a', 25, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (186, N'b', 25, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (204, N'a', 26, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (205, N'b', 26, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (206, N'c', 26, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (207, N'd', 26, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (210, N'a', 28, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (221, N'A. True', 2, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (222, N'B. False', 2, 0)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (223, N'A. Yes', 29, 1)
INSERT [dbo].[Answer] ([id], [content], [questionId], [status]) VALUES (224, N'B. No', 29, 0)
SET IDENTITY_INSERT [dbo].[Answer] OFF
SET IDENTITY_INSERT [dbo].[Blog] ON 

INSERT [dbo].[Blog] ([id], [subCategoryId], [authorId], [title], [updatedDate], [thumbnail], [briefInformation], [status], [featuring], [description]) VALUES (1, 4, 1, N'Welcome to Group 5''s Quiz Practicing System', CAST(N'2022-06-26' AS Date), N'img/blog/1.jpg', N'A grand welcome to all new users of our system', 1, N'1', N'Greetings.

We welcome you all to our latest project: A quiz practicing system. This grand opening comes with a large discount for all subjects.

It''s a pleasure to have you here.')
INSERT [dbo].[Blog] ([id], [subCategoryId], [authorId], [title], [updatedDate], [thumbnail], [briefInformation], [status], [featuring], [description]) VALUES (2, 2, 1, N'Big Opening Sale', CAST(N'2022-07-15' AS Date), N'img/blog/2.jpg', N'Announcing the grand opening sale', 1, N'0', N'We are having a sale in commemoration of our website opening.')
INSERT [dbo].[Blog] ([id], [subCategoryId], [authorId], [title], [updatedDate], [thumbnail], [briefInformation], [status], [featuring], [description]) VALUES (3, 1, 1, N'May''s Recommendations', CAST(N'2022-06-26' AS Date), N'img/blog/3.jpg', N'Introducing: Our recommended courses', 1, N'0', N'Hello everyone. Today I''ll introduce you some of our recommended courses.
Statistic and Probabilities
Computer Architecture and Organization')
INSERT [dbo].[Blog] ([id], [subCategoryId], [authorId], [title], [updatedDate], [thumbnail], [briefInformation], [status], [featuring], [description]) VALUES (4, 5, 1, N'Short Maintenance', CAST(N'2022-07-13' AS Date), N'img/blog/4.jpg', N'A quick update to our system', 1, N'0', N'We apologize for the inconvenience.
The maintenance will last for two hours.')
INSERT [dbo].[Blog] ([id], [subCategoryId], [authorId], [title], [updatedDate], [thumbnail], [briefInformation], [status], [featuring], [description]) VALUES (5, 2, 1, N'Buy One Get One', CAST(N'2022-07-13' AS Date), N'img/blog/5.jpg', N'Because two is better than one', 0, N'0', N'Buy one!
Get one!
Register subjects to your heart content with our ''Buy One Get One'' program.
You can register a subject package and choose another as a bonus (bonus package must not be more expensive than the bought one).
Please take this opportunity to register for all the subjects you are interested in.')
INSERT [dbo].[Blog] ([id], [subCategoryId], [authorId], [title], [updatedDate], [thumbnail], [briefInformation], [status], [featuring], [description]) VALUES (6, 6, 1, N'June''s Major Update', CAST(N'2022-07-07' AS Date), N'img/blog/6.jpg', N'Update new courses', 1, N'0', N'Greetings!
The new month comes with a large update to our subjects catelogue

SQL Programming
Python Programming
Psychology
C Programming
Writing English Researchs
Human Resource Management')
INSERT [dbo].[Blog] ([id], [subCategoryId], [authorId], [title], [updatedDate], [thumbnail], [briefInformation], [status], [featuring], [description]) VALUES (7, 1, 1, N'June''s Recommendation', CAST(N'2022-06-26' AS Date), N'img/blog/7.jpg', N'This month''s recommended courses', 1, N'1', N'It''s the start of a new month! We are excited to introduce you to our latest recommendations of June
Python Programming: The new darling of the programming community. Its ease of use and popularity will surely give you lots of benefits learning.
Writing English Researches: This course will give you a better idea of writing better. Maybe I should take it too.
Human Resource Management: You''re in a position of managing resource? You should take this course. Now.')
INSERT [dbo].[Blog] ([id], [subCategoryId], [authorId], [title], [updatedDate], [thumbnail], [briefInformation], [status], [featuring], [description]) VALUES (8, 1, 4, N'IT Support Group', CAST(N'2022-06-26' AS Date), N'img/blog/8.jpg', N'The new ISG at your service', 1, N'1', N'We are launching an all new IT Support Group to help our users with Website surfing.
Hotline: 09xxxxxxxxxx')
INSERT [dbo].[Blog] ([id], [subCategoryId], [authorId], [title], [updatedDate], [thumbnail], [briefInformation], [status], [featuring], [description]) VALUES (9, 4, 1, N'July Review', CAST(N'2022-07-15' AS Date), N'img/blog/9.jpg', N'We are holding a presentation on Wednesday!', 1, N'1', N'Greetings!

On the 20th of July, we will be holding a presentation of our project for SWP391.

We hope you''ll cheer for us.

Sincerely,
QPS')
SET IDENTITY_INSERT [dbo].[Blog] OFF
SET IDENTITY_INSERT [dbo].[Blog_Category] ON 

INSERT [dbo].[Blog_Category] ([id], [name]) VALUES (1, N'Public Relation')
INSERT [dbo].[Blog_Category] ([id], [name]) VALUES (2, N'News')
SET IDENTITY_INSERT [dbo].[Blog_Category] OFF
SET IDENTITY_INSERT [dbo].[Blog_Sub_Category] ON 

INSERT [dbo].[Blog_Sub_Category] ([id], [name], [categoryId]) VALUES (1, N'Recommendation', 1)
INSERT [dbo].[Blog_Sub_Category] ([id], [name], [categoryId]) VALUES (2, N'Sale', 1)
INSERT [dbo].[Blog_Sub_Category] ([id], [name], [categoryId]) VALUES (3, N'Service', 1)
INSERT [dbo].[Blog_Sub_Category] ([id], [name], [categoryId]) VALUES (4, N'Announcement', 2)
INSERT [dbo].[Blog_Sub_Category] ([id], [name], [categoryId]) VALUES (5, N'Maintenance', 2)
INSERT [dbo].[Blog_Sub_Category] ([id], [name], [categoryId]) VALUES (6, N'Update', 2)
INSERT [dbo].[Blog_Sub_Category] ([id], [name], [categoryId]) VALUES (7, N'Help', 1)
SET IDENTITY_INSERT [dbo].[Blog_Sub_Category] OFF
SET IDENTITY_INSERT [dbo].[Dimension] ON 

INSERT [dbo].[Dimension] ([id], [name], [description], [typeId], [subjectId]) VALUES (1, N'Mathematics', N'math', 1, 1)
INSERT [dbo].[Dimension] ([id], [name], [description], [typeId], [subjectId]) VALUES (2, N'Accounting', N'accounting', 1, 1)
INSERT [dbo].[Dimension] ([id], [name], [description], [typeId], [subjectId]) VALUES (3, N'Programming', N'programming', 1, 2)
INSERT [dbo].[Dimension] ([id], [name], [description], [typeId], [subjectId]) VALUES (4, N'Marketing', N'marketing', 1, 2)
SET IDENTITY_INSERT [dbo].[Dimension] OFF
SET IDENTITY_INSERT [dbo].[Dimension_Type] ON 

INSERT [dbo].[Dimension_Type] ([id], [name]) VALUES (1, N'Domain')
INSERT [dbo].[Dimension_Type] ([id], [name]) VALUES (2, N'Group')
SET IDENTITY_INSERT [dbo].[Dimension_Type] OFF
SET IDENTITY_INSERT [dbo].[Function] ON 

INSERT [dbo].[Function] ([id], [name], [url]) VALUES (1, N'PostsList', N'/postslist')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (2, N'PostDetails', N'/postdetails')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (3, N'RegistrationsList', N'/registrationslist')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (4, N'RegistrationDetails', N'/registrationdetails')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (5, N'QuestionsList', N'/questionslist')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (6, N'QuestionDetails', N'/questiondetails')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (7, N'QuizzesList', N'/quizzeslist')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (8, N'QuizDetails', N'/quizdetails')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (9, N'PostAction', N'/postaction')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (10, N'SliderAction', N'/slideraction')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (11, N'BlogSubCategoryCreate', N'/blogsubcategorycreate')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (12, N'SlidersList', N'/sliderslist')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (13, N'SliderDetails', N'/sliderdetails')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (14, N'AdminLessonsList', N'/adminlessonslist')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (15, N'PracticesList', N'/practiceslist')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (16, N'PracticeDetails', N'/practicedetails')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (17, N'SimulationExam', N'/simulationexam')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (18, N'ExamDetails', N'/examdetails')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (19, N'UsersList', N'/userslist')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (20, N'UserDetails', N'/userdetails')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (21, N'AdminSubjectsList', N'/adminsubjectslist')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (22, N'AdminSubjectDetails', N'/adminsubjectdetails')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (23, N'CreateLesson', N'/createlesson')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (24, N'AdminLessonDetails', N'/adminlessondetails')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (25, N'CreatePost', N'/createpost')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (26, N'CreateSlider', N'/createslider')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (27, N'LessonsList', N'/lessonslist')
INSERT [dbo].[Function] ([id], [name], [url]) VALUES (28, N'LessonDetails', N'/lessondetails')
SET IDENTITY_INSERT [dbo].[Function] OFF
INSERT [dbo].[Function_Role] ([functionId], [roleId]) VALUES (1, 1)
INSERT [dbo].[Function_Role] ([functionId], [roleId]) VALUES (2, 1)
INSERT [dbo].[Function_Role] ([functionId], [roleId]) VALUES (5, 1)
INSERT [dbo].[Function_Role] ([functionId], [roleId]) VALUES (6, 1)
INSERT [dbo].[Function_Role] ([functionId], [roleId]) VALUES (7, 1)
INSERT [dbo].[Function_Role] ([functionId], [roleId]) VALUES (8, 1)
SET IDENTITY_INSERT [dbo].[Lesson] ON 

INSERT [dbo].[Lesson] ([id], [subjectId], [order], [typeId], [status], [subjectTopicId], [lessonContentId], [quizId], [name]) VALUES (18, 1, CAST(1.00 AS Decimal(5, 2)), 1, 1, 4, NULL, NULL, N'Chapter 111')
INSERT [dbo].[Lesson] ([id], [subjectId], [order], [typeId], [status], [subjectTopicId], [lessonContentId], [quizId], [name]) VALUES (20, 1, CAST(1.12 AS Decimal(5, 2)), 3, 1, 4, NULL, 11, N'test32')
INSERT [dbo].[Lesson] ([id], [subjectId], [order], [typeId], [status], [subjectTopicId], [lessonContentId], [quizId], [name]) VALUES (21, 1, CAST(9.00 AS Decimal(5, 2)), 1, 1, 9, NULL, NULL, N'Chapter 192')
INSERT [dbo].[Lesson] ([id], [subjectId], [order], [typeId], [status], [subjectTopicId], [lessonContentId], [quizId], [name]) VALUES (22, 1, CAST(1.00 AS Decimal(5, 2)), 1, 1, 10, NULL, NULL, N'test rename')
INSERT [dbo].[Lesson] ([id], [subjectId], [order], [typeId], [status], [subjectTopicId], [lessonContentId], [quizId], [name]) VALUES (26, 2, CAST(123.00 AS Decimal(5, 2)), 1, 1, 12, NULL, NULL, N'123')
INSERT [dbo].[Lesson] ([id], [subjectId], [order], [typeId], [status], [subjectTopicId], [lessonContentId], [quizId], [name]) VALUES (27, 2, CAST(123.00 AS Decimal(5, 2)), 2, 1, 12, 3, NULL, N'456')
INSERT [dbo].[Lesson] ([id], [subjectId], [order], [typeId], [status], [subjectTopicId], [lessonContentId], [quizId], [name]) VALUES (30, 2, CAST(666.00 AS Decimal(5, 2)), 1, 1, 14, NULL, NULL, N'1st')
INSERT [dbo].[Lesson] ([id], [subjectId], [order], [typeId], [status], [subjectTopicId], [lessonContentId], [quizId], [name]) VALUES (31, 2, CAST(1.00 AS Decimal(5, 2)), 2, 1, 14, 4, NULL, N'2nd')
INSERT [dbo].[Lesson] ([id], [subjectId], [order], [typeId], [status], [subjectTopicId], [lessonContentId], [quizId], [name]) VALUES (32, 1, CAST(1.00 AS Decimal(5, 2)), 2, 1, 4, 5, NULL, N'MAS')
INSERT [dbo].[Lesson] ([id], [subjectId], [order], [typeId], [status], [subjectTopicId], [lessonContentId], [quizId], [name]) VALUES (33, 1, CAST(69.00 AS Decimal(5, 2)), 3, 1, 10, NULL, 9, N'Test Quiz')
SET IDENTITY_INSERT [dbo].[Lesson] OFF
SET IDENTITY_INSERT [dbo].[Lesson_Content] ON 

INSERT [dbo].[Lesson_Content] ([id], [htmlContent]) VALUES (2, N'<figure class="image"><img src="img/lesson/DiGhcyY9QR.jpg"></figure>')
INSERT [dbo].[Lesson_Content] ([id], [htmlContent]) VALUES (3, N'')
INSERT [dbo].[Lesson_Content] ([id], [htmlContent]) VALUES (4, N'<p>oanoinafoi</p>')
INSERT [dbo].[Lesson_Content] ([id], [htmlContent]) VALUES (5, N'<figure class="image"><img src="https://study.com/cimages/course-image/common-core-math-grade-6-statistics-probability-standards_186059_large.jpg" alt="Common Core Math Grade 6 - Statistics &amp; Probability: Standards Course -  Online Video Lessons | Study.com"><figcaption>Calculator</figcaption></figure><p>Some tips and tricks for MAS</p>')
SET IDENTITY_INSERT [dbo].[Lesson_Content] OFF
SET IDENTITY_INSERT [dbo].[Lesson_Type] ON 

INSERT [dbo].[Lesson_Type] ([id], [name], [column]) VALUES (1, N'Subject Topic', N'subjectTopicId')
INSERT [dbo].[Lesson_Type] ([id], [name], [column]) VALUES (2, N'Lesson Content', N'lessonContentId')
INSERT [dbo].[Lesson_Type] ([id], [name], [column]) VALUES (3, N'Quiz', N'quizId')
SET IDENTITY_INSERT [dbo].[Lesson_Type] OFF
SET IDENTITY_INSERT [dbo].[Level] ON 

INSERT [dbo].[Level] ([id], [name]) VALUES (1, N'Easy')
INSERT [dbo].[Level] ([id], [name]) VALUES (2, N'Normal')
INSERT [dbo].[Level] ([id], [name]) VALUES (3, N'Hard')
SET IDENTITY_INSERT [dbo].[Level] OFF
SET IDENTITY_INSERT [dbo].[Media_Type] ON 

INSERT [dbo].[Media_Type] ([id], [name]) VALUES (1, N'img')
INSERT [dbo].[Media_Type] ([id], [name]) VALUES (2, N'audio')
INSERT [dbo].[Media_Type] ([id], [name]) VALUES (3, N'video')
SET IDENTITY_INSERT [dbo].[Media_Type] OFF
INSERT [dbo].[New_Customers] ([register], [bought], [registrationdate]) VALUES (1, 2, CAST(N'2022-07-10' AS Date))
INSERT [dbo].[New_Customers] ([register], [bought], [registrationdate]) VALUES (2, 1, CAST(N'2022-07-11' AS Date))
INSERT [dbo].[New_Customers] ([register], [bought], [registrationdate]) VALUES (3, 1, CAST(N'2022-07-12' AS Date))
INSERT [dbo].[New_Customers] ([register], [bought], [registrationdate]) VALUES (2, 2, CAST(N'2022-07-13' AS Date))
INSERT [dbo].[New_Customers] ([register], [bought], [registrationdate]) VALUES (3, 1, CAST(N'2022-07-14' AS Date))
INSERT [dbo].[New_Customers] ([register], [bought], [registrationdate]) VALUES (3, 2, CAST(N'2022-07-15' AS Date))
INSERT [dbo].[New_Customers] ([register], [bought], [registrationdate]) VALUES (1, 1, CAST(N'2022-07-16' AS Date))
SET IDENTITY_INSERT [dbo].[Price_Package] ON 

INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (1, 9999, N'3 month package', CAST(3600.00 AS Decimal(10, 2)), CAST(3200.00 AS Decimal(10, 2)), 1, 1, N'3 month')
INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (2, 6, N'6 month package', CAST(6500.00 AS Decimal(10, 2)), CAST(6000.00 AS Decimal(10, 2)), 1, 1, N'6 month')
INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (3, 3, N'3 month package', CAST(3600.00 AS Decimal(10, 2)), CAST(3100.00 AS Decimal(10, 2)), 2, 1, N'3 month')
INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (4, 6, N'6 month package', CAST(6200.00 AS Decimal(10, 2)), CAST(6000.00 AS Decimal(10, 2)), 2, 0, N'6 month')
INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (5, 12, N'1 year package', CAST(11600.00 AS Decimal(10, 2)), CAST(8000.00 AS Decimal(10, 2)), 2, 1, N'1 year')
INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (12, 3, N'3 month package', CAST(2800.00 AS Decimal(10, 2)), CAST(2000.00 AS Decimal(10, 2)), 10, 1, N'3 month')
INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (14, 3, N'3 month package', CAST(3500.00 AS Decimal(10, 2)), CAST(2900.00 AS Decimal(10, 2)), 14, 1, N'3 month')
INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (15, 6, N'6 month package', CAST(5500.00 AS Decimal(10, 2)), CAST(4800.00 AS Decimal(10, 2)), 16, 1, N'6 month')
INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (16, 3, N'3 month package', CAST(3000.00 AS Decimal(10, 2)), CAST(2800.00 AS Decimal(10, 2)), 17, 1, N'3 month')
INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (17, 6, N'6 month package', CAST(7900.00 AS Decimal(10, 2)), CAST(7100.00 AS Decimal(10, 2)), 18, 1, N'6 month')
INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (18, 3, N'3 month package', CAST(2900.00 AS Decimal(10, 2)), CAST(2100.00 AS Decimal(10, 2)), 20, 1, N'3 month')
INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (19, 69, N'69 months', CAST(420.00 AS Decimal(10, 2)), CAST(69.00 AS Decimal(10, 2)), 10, 1, N'Blaze it')
INSERT [dbo].[Price_Package] ([id], [duration], [name], [listPrice], [salePrice], [subjectId], [status], [description]) VALUES (22, 3, N'3 month package', CAST(3000.00 AS Decimal(10, 2)), CAST(2800.00 AS Decimal(10, 2)), 22, 1, N'')
SET IDENTITY_INSERT [dbo].[Price_Package] OFF
SET IDENTITY_INSERT [dbo].[Question] ON 

INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (1, N'An employee at the local ice cream parlor asks three customers if they like chocolate ice cream. What is the population?', 17, 1, 0, N'img/question/q1', 1, NULL, 18, NULL)
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (2, N'Handsome?', 1, 3, 1, N'img/question/q2', 1, 1, 18, N'asd')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (3, N'1+1 = ?', 1, 3, 1, N'img/question/q3', 1, 1, 18, N'2 or 10')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (6, N'According to a survey of American households, the probability that the residents own 2 cars if annual household income is over $35,000 is 70%. Of the households surveyed, 50% had incomes over $35,000 and 80% had 2 cars. The probability that the residents of a household do not own 2 cars and have an income over $35,000 a year is:', 1, 1, 1, N'img/question/q4', 1, 1, 18, N'a little peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (7, N'1NF 2NF 3NF ?', 17, 3, 1, N'img/question/itsp.jpg', 1, 1, 18, N'3NF')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (8, N'dsadsad', 16, 1, 0, N'img/question/itsp.jpg', 1, 1, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (9, N'asdsadsadsa', 20, 1, 1, N'img/question/q5', 1, 1, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (10, N'test', 16, 3, 1, NULL, NULL, 1, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (11, N'sadsadsaads', 20, 2, 1, N'img/question/q11', 1, 1, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (12, N'sadsa', 16, 3, 1, N'img/question/q12', 1, 1, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (13, N'zxzxz', 16, 2, 1, N'img/question/q13', 1, 1, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (14, N'Python is good?', 16, 2, 1, N'img/question/q14', 1, 3, 18, N'Maybe')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (15, N'test', 17, 3, 1, NULL, NULL, 1, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (16, N'PK or FK ?', 17, 3, 1, N'img/question/q16', 1, 1, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (17, N'print or println ?', 16, 3, 1, N'img/question/q17', 1, 3, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (19, N'', 17, 3, 1, NULL, NULL, 1, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (20, N'g', 17, 3, 1, NULL, NULL, 1, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (21, N'testste', 17, 3, 1, NULL, NULL, 1, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (22, N'xzcxzcx', 16, 2, 1, N'img/question/q22', 2, 1, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (23, N'sadasdassadas', 1, 1, 1, NULL, NULL, 1, 18, N'what')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (24, N'sdsadasdsa', 1, 1, 0, NULL, NULL, 2, 18, N'test peek')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (25, N'test', 1, 1, 1, N'img/question/q25', 2, 1, NULL, NULL)
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (26, N'sadsada', 1, 2, 1, N'img/question/q26', 2, 1, 18, N'samdsammdsa')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (27, N'', 1, 1, 1, NULL, NULL, 1, NULL, N'')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (28, N'sd', 1, 1, 1, N'img/question/q28', 1, 1, 18, N'a')
INSERT [dbo].[Question] ([id], [content], [subjectId], [levelId], [status], [media], [mediaTypeId], [dimensionId], [lessonId], [explanation]) VALUES (29, N'Review Phim', 1, 1, 1, N'https://www.youtube.com/embed/0eE7c_viVgw', 3, 1, 18, N'Test vid')
SET IDENTITY_INSERT [dbo].[Question] OFF
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (1, 2, 1)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (1, 3, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (1, 6, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (1, 23, 1)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (1, 24, 1)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (2, 1, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (2, 2, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (2, 3, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (2, 6, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (2, 7, 1)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (3, 2, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (3, 3, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (3, 6, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (3, 23, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (3, 24, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (3, 25, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (3, 26, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (3, 27, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (3, 28, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (5, 1, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (5, 2, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (5, 3, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (5, 6, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (5, 7, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (6, 1, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (6, 2, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (6, 3, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (6, 6, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (6, 7, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (7, 2, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (7, 3, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (7, 6, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (7, 23, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (7, 25, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (7, 26, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (7, 27, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (7, 28, 0)
INSERT [dbo].[Question_History] ([quizHistoryId], [questionId], [markStatus]) VALUES (7, 29, 0)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (1, 1)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (1, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (1, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (1, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (1, 7)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (2, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (2, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (3, 1)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (3, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (3, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (3, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (3, 7)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (4, 1)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (4, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (4, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (4, 7)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (9, 1)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (9, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (9, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (9, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (9, 7)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (10, 1)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (10, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (10, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (10, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (11, 1)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (11, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (11, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (11, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (11, 7)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (11, 8)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (13, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (13, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (13, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (13, 23)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (13, 24)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (14, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (14, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (14, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (14, 23)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (14, 24)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (15, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (15, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (15, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (15, 23)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (15, 24)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (16, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (16, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (16, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (16, 23)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (16, 24)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (17, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (17, 24)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (18, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (18, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (18, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (18, 23)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (18, 24)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (19, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (19, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (19, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (19, 23)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (19, 24)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (20, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (20, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (20, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (20, 23)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (20, 24)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (21, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (21, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (21, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (21, 23)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (21, 24)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (22, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (22, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (22, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (22, 23)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (22, 24)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (22, 25)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (22, 26)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (22, 27)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (22, 28)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (23, 2)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (23, 3)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (23, 6)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (23, 23)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (23, 25)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (23, 26)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (23, 27)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (23, 28)
INSERT [dbo].[Question_Quiz] ([quizId], [questionId]) VALUES (23, 29)
SET IDENTITY_INSERT [dbo].[Quiz] ON 

INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (1, N'Exam 1', 1, 3, 60, CAST(50 AS Decimal(3, 0)), 1, N'Quiz 1')
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (2, N'Exam 2', 1, 3, 60, CAST(80 AS Decimal(3, 0)), 1, N'Quiz 2')
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (3, N'Exam 3', 1, 3, 60, CAST(90 AS Decimal(3, 0)), 1, N'Quiz about human')
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (4, N'C - Final', 1, 3, 50, CAST(50 AS Decimal(3, 0)), 1, N'Final Exam')
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (5, N'Python 1', 1, 3, 40, CAST(89 AS Decimal(3, 0)), 1, N'Test 1')
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (6, N'SQL 1', 1, 3, 30, CAST(77 AS Decimal(3, 0)), 1, N'Test 1')
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (7, N'Wirting Email', 1, 3, 30, CAST(80 AS Decimal(3, 0)), 1, N'Test 2')
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (8, N'Exam 4', 1, 3, 20, CAST(75 AS Decimal(3, 0)), 1, N'Midterm Exam')
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (9, N'Test 12', 1, 1, 1, CAST(50 AS Decimal(3, 0)), 2, N'Test 1')
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (10, N'Test 2', 1, 2, 60, CAST(75 AS Decimal(3, 0)), 2, N'Test 2')
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (11, N'Test 3', 1, 2, 60, CAST(70 AS Decimal(3, 0)), 2, N'Test 3')
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (12, N'F - Final', 2, 3, 15, CAST(12 AS Decimal(3, 0)), 1, N'Final Exam F')
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (13, NULL, 1, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (14, NULL, 1, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (15, NULL, 1, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (16, NULL, 1, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (17, NULL, 1, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (18, NULL, 1, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (19, NULL, 1, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (20, NULL, 1, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (21, NULL, 1, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (22, NULL, 1, NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[Quiz] ([id], [name], [subjectId], [levelId], [duration], [passRate], [typeId], [description]) VALUES (23, NULL, 1, NULL, NULL, NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[Quiz] OFF
SET IDENTITY_INSERT [dbo].[Quiz_History] ON 

INSERT [dbo].[Quiz_History] ([id], [quizId], [userId], [timeStarted], [timeFinished], [subjectId]) VALUES (1, 21, 1, CAST(N'2022-07-03 19:21:54.000' AS DateTime), CAST(N'2022-07-10 14:31:55.937' AS DateTime), 1)
INSERT [dbo].[Quiz_History] ([id], [quizId], [userId], [timeStarted], [timeFinished], [subjectId]) VALUES (2, 9, 1, CAST(N'2022-07-11 12:31:55.000' AS DateTime), CAST(N'2022-07-11 12:34:01.637' AS DateTime), 1)
INSERT [dbo].[Quiz_History] ([id], [quizId], [userId], [timeStarted], [timeFinished], [subjectId]) VALUES (3, 22, 1, CAST(N'2022-07-11 12:34:26.000' AS DateTime), NULL, 1)
INSERT [dbo].[Quiz_History] ([id], [quizId], [userId], [timeStarted], [timeFinished], [subjectId]) VALUES (5, 9, 1, CAST(N'2022-07-14 22:15:42.000' AS DateTime), NULL, 1)
INSERT [dbo].[Quiz_History] ([id], [quizId], [userId], [timeStarted], [timeFinished], [subjectId]) VALUES (6, 9, 1, CAST(N'2022-07-14 22:18:19.000' AS DateTime), NULL, 1)
INSERT [dbo].[Quiz_History] ([id], [quizId], [userId], [timeStarted], [timeFinished], [subjectId]) VALUES (7, 23, 1, CAST(N'2022-07-15 21:33:24.000' AS DateTime), NULL, 1)
SET IDENTITY_INSERT [dbo].[Quiz_History] OFF
SET IDENTITY_INSERT [dbo].[Quiz_Type] ON 

INSERT [dbo].[Quiz_Type] ([id], [name]) VALUES (1, N'Practice')
INSERT [dbo].[Quiz_Type] ([id], [name]) VALUES (2, N'Exam')
SET IDENTITY_INSERT [dbo].[Quiz_Type] OFF
SET IDENTITY_INSERT [dbo].[Registration] ON 

INSERT [dbo].[Registration] ([id], [subjectId], [email], [packageId], [registrationTime], [totalCost], [status], [validFrom], [lastUpdatedBy], [note], [sale], [validTo]) VALUES (3, 1, N'phuongndhe160465@fpt.edu.vn', 1, CAST(N'2022-06-13 17:23:12.000' AS DateTime), CAST(6000.00 AS Decimal(10, 2)), 1, CAST(N'2022-06-13' AS Date), 7, N'paid already wtf man', 0, CAST(N'2022-09-13' AS Date))
INSERT [dbo].[Registration] ([id], [subjectId], [email], [packageId], [registrationTime], [totalCost], [status], [validFrom], [lastUpdatedBy], [note], [sale], [validTo]) VALUES (4, 2, N'phuongndhe160465@fpt.edu.vn', 3, CAST(N'2022-06-13 18:46:38.000' AS DateTime), CAST(3100.00 AS Decimal(10, 2)), 1, CAST(N'2022-06-13' AS Date), NULL, N'not paid', 0, CAST(N'2022-09-13' AS Date))
INSERT [dbo].[Registration] ([id], [subjectId], [email], [packageId], [registrationTime], [totalCost], [status], [validFrom], [lastUpdatedBy], [note], [sale], [validTo]) VALUES (5, 17, N'phuongndhe160465@fpt.edu.vn', 16, CAST(N'2022-06-14 20:35:48.000' AS DateTime), CAST(2800.00 AS Decimal(10, 2)), 1, CAST(N'2022-06-14' AS Date), 1, N'', 0, CAST(N'2022-09-14' AS Date))
INSERT [dbo].[Registration] ([id], [subjectId], [email], [packageId], [registrationTime], [totalCost], [status], [validFrom], [lastUpdatedBy], [note], [sale], [validTo]) VALUES (6, 16, N'phuongndhe160465@fpt.edu.vn', 15, CAST(N'2022-06-14 20:36:02.000' AS DateTime), CAST(4800.00 AS Decimal(10, 2)), 0, CAST(N'2022-06-14' AS Date), 7, N'paid already wtf', 0, CAST(N'2022-12-14' AS Date))
INSERT [dbo].[Registration] ([id], [subjectId], [email], [packageId], [registrationTime], [totalCost], [status], [validFrom], [lastUpdatedBy], [note], [sale], [validTo]) VALUES (7, 1, N'test', 1, CAST(N'2022-06-16 22:26:31.000' AS DateTime), CAST(3200.00 AS Decimal(10, 2)), 0, CAST(N'2022-06-16' AS Date), 9, N'', 0, CAST(N'2022-06-24' AS Date))
INSERT [dbo].[Registration] ([id], [subjectId], [email], [packageId], [registrationTime], [totalCost], [status], [validFrom], [lastUpdatedBy], [note], [sale], [validTo]) VALUES (8, 1, N'test', 1, CAST(N'2022-01-01 01:01:01.000' AS DateTime), CAST(1.00 AS Decimal(10, 2)), 1, CAST(N'2022-01-01' AS Date), 1, N'test', 1, CAST(N'2022-12-24' AS Date))
INSERT [dbo].[Registration] ([id], [subjectId], [email], [packageId], [registrationTime], [totalCost], [status], [validFrom], [lastUpdatedBy], [note], [sale], [validTo]) VALUES (11, 1, N'subaccfordownloadinggames@gmail.com', 1, CAST(N'2022-06-17 10:16:11.000' AS DateTime), CAST(3200.00 AS Decimal(10, 2)), 1, CAST(N'2022-06-23' AS Date), 10, N'oioioij', 0, CAST(N'2022-06-30' AS Date))
INSERT [dbo].[Registration] ([id], [subjectId], [email], [packageId], [registrationTime], [totalCost], [status], [validFrom], [lastUpdatedBy], [note], [sale], [validTo]) VALUES (12, 10, N'phuongndhe160465@fpt.edu.vn', 19, CAST(N'2022-07-07 21:47:44.000' AS DateTime), CAST(69.00 AS Decimal(10, 2)), 0, CAST(N'2022-07-07' AS Date), NULL, N'', 0, CAST(N'2028-04-07' AS Date))
SET IDENTITY_INSERT [dbo].[Registration] OFF
INSERT [dbo].[Registrations_By_Day] ([registrationdate], [paid], [total]) VALUES (CAST(N'2022-07-05' AS Date), 5, 6)
INSERT [dbo].[Registrations_By_Day] ([registrationdate], [paid], [total]) VALUES (CAST(N'2022-07-06' AS Date), 7, 8)
INSERT [dbo].[Registrations_By_Day] ([registrationdate], [paid], [total]) VALUES (CAST(N'2022-07-07' AS Date), 2, 6)
INSERT [dbo].[Registrations_By_Day] ([registrationdate], [paid], [total]) VALUES (CAST(N'2022-07-08' AS Date), 3, 4)
INSERT [dbo].[Registrations_By_Day] ([registrationdate], [paid], [total]) VALUES (CAST(N'2022-07-09' AS Date), 2, 2)
INSERT [dbo].[Registrations_By_Day] ([registrationdate], [paid], [total]) VALUES (CAST(N'2022-07-10' AS Date), 3, 5)
INSERT [dbo].[Registrations_By_Day] ([registrationdate], [paid], [total]) VALUES (CAST(N'2022-07-11' AS Date), 1, 3)
INSERT [dbo].[Registrations_By_Day] ([registrationdate], [paid], [total]) VALUES (CAST(N'2022-07-12' AS Date), 4, 5)
INSERT [dbo].[Registrations_By_Day] ([registrationdate], [paid], [total]) VALUES (CAST(N'2022-07-13' AS Date), 1, 2)
INSERT [dbo].[Registrations_By_Day] ([registrationdate], [paid], [total]) VALUES (CAST(N'2022-07-14' AS Date), 8, 10)
INSERT [dbo].[Registrations_By_Day] ([registrationdate], [paid], [total]) VALUES (CAST(N'2022-07-15' AS Date), 4, 12)
INSERT [dbo].[Registrations_By_Day] ([registrationdate], [paid], [total]) VALUES (CAST(N'2022-07-16' AS Date), 3, 3)
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([id], [name]) VALUES (1, N'Admin')
INSERT [dbo].[Role] ([id], [name]) VALUES (2, N'Customer')
INSERT [dbo].[Role] ([id], [name]) VALUES (3, N'Sale')
INSERT [dbo].[Role] ([id], [name]) VALUES (4, N'Marketing')
INSERT [dbo].[Role] ([id], [name]) VALUES (5, N'Expert')
SET IDENTITY_INSERT [dbo].[Role] OFF
SET IDENTITY_INSERT [dbo].[Slider] ON 

INSERT [dbo].[Slider] ([id], [title], [thumbnail], [backlink], [status], [note]) VALUES (1, N'Welcome to Group 5''s Quiz Practicing System', N'img/blog/1.jpg', N'blogdetails?blogId=1', 1, N'')
INSERT [dbo].[Slider] ([id], [title], [thumbnail], [backlink], [status], [note]) VALUES (2, N'July Review', N'img/blog/2.jpg', N'blogdetails?blogId=9', 1, N'')
SET IDENTITY_INSERT [dbo].[Slider] OFF
SET IDENTITY_INSERT [dbo].[Subject] ON 

INSERT [dbo].[Subject] ([id], [title], [tagLine], [subCategoryId], [thumbnail], [description], [ownerId], [status], [updatedDate], [featured], [createdDate]) VALUES (1, N'Statistic and Probability', N'Think Probabilities, Learn Statistics.', 1, N'img/subject/1.jpg', N'Probability And Statistics are the two important concepts in Math.
Probability is all about chance. Whereas statistics is more about how we handle various data using different techniques.
The statistic has a huge application nowadays in data science professions.
The professionals use the stats and do the predictions of the business. It helps them to predict the future profit or loss attained by the company.
Probability denotes the possibility of the outcome of any random event. The meaning of this term is to check the extent to which any event is likely to happen.', 8, 1, CAST(N'2022-07-15' AS Date), 1, CAST(N'2022-07-06' AS Date))
INSERT [dbo].[Subject] ([id], [title], [tagLine], [subCategoryId], [thumbnail], [description], [ownerId], [status], [updatedDate], [featured], [createdDate]) VALUES (2, N'Computer Architecture and Organization', N'The mother art is architecture', 1, N'img/subject/2.jpg', N'Computer Organization and Architecture is the study of internal working, structuring, and implementation of a computer system. Architecture in the computer system, same as anywhere else, refers to the externally visual attributes of the system. Externally visual attributes, here in computer science, mean the way a system is visible to the logic of programs (not the human eyes!). Organization of a computer system is the way of practical implementation that results in the realization of architectural specifications of a computer system.[1] In more general language, the Architecture of a computer system can be considered as a catalog of tools available for any operator using the system, while Organization will be the way the system is structured so that all those cataloged tools can be used, and efficiently.', 12, 1, CAST(N'2022-07-07' AS Date), 1, CAST(N'2022-07-06' AS Date))
INSERT [dbo].[Subject] ([id], [title], [tagLine], [subCategoryId], [thumbnail], [description], [ownerId], [status], [updatedDate], [featured], [createdDate]) VALUES (10, N'Human Resouce Management', N'A company''s most valuable asset is its employees.', 2, N'img/subject/10.png', N'Human resource management (HRM or HR) is the strategic approach to the effective and efficient management of people in a company or organization such that they help their business gain a competitive advantage', 8, 1, CAST(N'2022-07-07' AS Date), 0, CAST(N'2022-07-06' AS Date))
INSERT [dbo].[Subject] ([id], [title], [tagLine], [subCategoryId], [thumbnail], [description], [ownerId], [status], [updatedDate], [featured], [createdDate]) VALUES (14, N'C Programming', N'Begin to program with the first language', 7, N'img/subject/14.png', N'C is a powerful general-purpose programming language. It can be used to develop software like operating systems, databases, compilers, and so on. C programming is an excellent language to learn to program for beginners.', 12, 1, CAST(N'2022-07-07' AS Date), 1, CAST(N'2022-07-06' AS Date))
INSERT [dbo].[Subject] ([id], [title], [tagLine], [subCategoryId], [thumbnail], [description], [ownerId], [status], [updatedDate], [featured], [createdDate]) VALUES (16, N'Python Programming', N'The easiest language to learn', 5, N'img/subject/16.webp', N'Python is a high-level, interpreted, general-purpose programming language. Its design philosophy emphasizes code readability with the use of significant indentation. Python is dynamically-typed and garbage-collected. It supports multiple programming paradigms, including structured (particularly procedural), object-oriented and functional programming. It is often described as a batteries included language due to its comprehensive standard library', 8, 1, CAST(N'2022-07-07' AS Date), 0, CAST(N'2022-07-06' AS Date))
INSERT [dbo].[Subject] ([id], [title], [tagLine], [subCategoryId], [thumbnail], [description], [ownerId], [status], [updatedDate], [featured], [createdDate]) VALUES (17, N'SQL Programming', N'Search everything with queries', 6, N'img/subject/17.jpg', N'Structured Query Language (SQL) is a standardized programming language that is used to manage relational databases and perform various operations on the data in them.', 12, 1, CAST(N'2022-07-07' AS Date), 1, CAST(N'2022-07-06' AS Date))
INSERT [dbo].[Subject] ([id], [title], [tagLine], [subCategoryId], [thumbnail], [description], [ownerId], [status], [updatedDate], [featured], [createdDate]) VALUES (18, N'Writing English Researchs', N'Writing is not as hard as it looks', 11, N'img/subject/18.jpg', N'Sometimes the most difficult part of writing a research paper is just getting it started. Contained in this packet, you will
find a list of six steps that will aid you in the research paper writing process. You may develop your own steps or
procedures as you progress in your writing career; these steps are just to help you begin.', 8, 1, CAST(N'2022-07-07' AS Date), 1, CAST(N'2022-07-06' AS Date))
INSERT [dbo].[Subject] ([id], [title], [tagLine], [subCategoryId], [thumbnail], [description], [ownerId], [status], [updatedDate], [featured], [createdDate]) VALUES (20, N'Psychology', N'Manipulate people''s mind for a good purpose', 1, N'img/subject/20.jpg', N'Psychology is the scientific study of mind and behavior. Psychology includes the study of conscious and unconscious phenomena, including feelings and thoughts. It is an academic discipline of immense scope, crossing the boundaries between the natural and social sciences. Psychologists seek an understanding of the emergent properties of brains, linking the discipline to neuroscience. ', 8, 1, CAST(N'2022-07-07' AS Date), 1, CAST(N'2022-07-06' AS Date))
INSERT [dbo].[Subject] ([id], [title], [tagLine], [subCategoryId], [thumbnail], [description], [ownerId], [status], [updatedDate], [featured], [createdDate]) VALUES (22, N'Ethics in IT', N'The needed ethics for IT engineers', 3, N'img/subject/0.webp', N'Organizations and governments are seeking out ethics professionals to minimize risk and guide their decision-making about the design of inclusive, responsible, and trusted technology. An algorithm not designed and assessed in alignment with ethical standards can create further inequity across race, gender and marginalized populations. The reputational and financial impact of an ethics violation can devastate a company. Knowledgeable ethics leaders are needed who can navigate through the more than 160 frameworks and guidelines to select and implement the best strategy to promote fairness and minimize risk for their organization. This specialization is designed for learners who want to create and lead initiatives that prioritize ethical integrity within emerging data-driven technology fields such as artificial intelligence and data science and will be prepared to bridge the gap between theory and practice.', 12, 1, CAST(N'2022-07-15' AS Date), 1, CAST(N'2022-07-06' AS Date))
SET IDENTITY_INSERT [dbo].[Subject] OFF
SET IDENTITY_INSERT [dbo].[Subject_Category] ON 

INSERT [dbo].[Subject_Category] ([id], [name]) VALUES (1, N'University')
INSERT [dbo].[Subject_Category] ([id], [name]) VALUES (2, N'High School')
INSERT [dbo].[Subject_Category] ([id], [name]) VALUES (3, N'Information Technology')
INSERT [dbo].[Subject_Category] ([id], [name]) VALUES (4, N'Law')
INSERT [dbo].[Subject_Category] ([id], [name]) VALUES (5, N'Medical')
INSERT [dbo].[Subject_Category] ([id], [name]) VALUES (6, N'Language')
SET IDENTITY_INSERT [dbo].[Subject_Category] OFF
SET IDENTITY_INSERT [dbo].[Subject_Sub_Category] ON 

INSERT [dbo].[Subject_Sub_Category] ([id], [name], [categoryId]) VALUES (1, N'Accounting & Finance', 1)
INSERT [dbo].[Subject_Sub_Category] ([id], [name], [categoryId]) VALUES (2, N'Art & Design', 2)
INSERT [dbo].[Subject_Sub_Category] ([id], [name], [categoryId]) VALUES (3, N'Information Technology', 1)
INSERT [dbo].[Subject_Sub_Category] ([id], [name], [categoryId]) VALUES (4, N'Architecture Desgin', 2)
INSERT [dbo].[Subject_Sub_Category] ([id], [name], [categoryId]) VALUES (5, N'Artificial Intelligent', 3)
INSERT [dbo].[Subject_Sub_Category] ([id], [name], [categoryId]) VALUES (6, N'Database System', 3)
INSERT [dbo].[Subject_Sub_Category] ([id], [name], [categoryId]) VALUES (7, N'Software Engineer', 3)
INSERT [dbo].[Subject_Sub_Category] ([id], [name], [categoryId]) VALUES (8, N'Information Security', 3)
INSERT [dbo].[Subject_Sub_Category] ([id], [name], [categoryId]) VALUES (9, N'Physical Health', 5)
INSERT [dbo].[Subject_Sub_Category] ([id], [name], [categoryId]) VALUES (10, N'Mental Health', 5)
INSERT [dbo].[Subject_Sub_Category] ([id], [name], [categoryId]) VALUES (11, N'English Language', 6)
INSERT [dbo].[Subject_Sub_Category] ([id], [name], [categoryId]) VALUES (12, N'Japanese Language', 6)
SET IDENTITY_INSERT [dbo].[Subject_Sub_Category] OFF
SET IDENTITY_INSERT [dbo].[Subject_Topic] ON 

INSERT [dbo].[Subject_Topic] ([id], [name]) VALUES (4, N'Chapter 111')
INSERT [dbo].[Subject_Topic] ([id], [name]) VALUES (9, N'Chapter 192')
INSERT [dbo].[Subject_Topic] ([id], [name]) VALUES (10, N'test rename')
INSERT [dbo].[Subject_Topic] ([id], [name]) VALUES (11, N'')
INSERT [dbo].[Subject_Topic] ([id], [name]) VALUES (12, N'123')
INSERT [dbo].[Subject_Topic] ([id], [name]) VALUES (13, N'')
INSERT [dbo].[Subject_Topic] ([id], [name]) VALUES (14, N'1st')
SET IDENTITY_INSERT [dbo].[Subject_Topic] OFF
INSERT [dbo].[Total_Registration] ([submitted], [cancel], [success]) VALUES (10, 2, 21)
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([id], [email], [name], [gender], [phone], [status], [address], [avatar]) VALUES (1, N'phuongndhe160465@fpt.edu.vn', N'Nguyen Duy Phuong', 1, N'9999999999', 1, N'Tan Xaaa', N'img/user/phuongndhe160465@fpt.edu.vn.jpg')
INSERT [dbo].[User] ([id], [email], [name], [gender], [phone], [status], [address], [avatar]) VALUES (4, N'bachhhhe160470@fpt.edu.vn', N'Ha Huy Bach', 1, N'0988888888', 1, N'Ba Dinh Ha Noi', N'img/ava.jpg')
INSERT [dbo].[User] ([id], [email], [name], [gender], [phone], [status], [address], [avatar]) VALUES (7, N'huy69420@fpt.edu.vn', N'Do Quang Huy', 1, N'0999999999', 0, N'iojweaifjpiafejpoajefpo', N'')
INSERT [dbo].[User] ([id], [email], [name], [gender], [phone], [status], [address], [avatar]) VALUES (8, N'thang96420@fpt.edu.vn', N'Nguyen An Thang', 1, N'77887', 1, N'Hyen Ung', N'')
INSERT [dbo].[User] ([id], [email], [name], [gender], [phone], [status], [address], [avatar]) VALUES (9, N'abc@fpt.edu.vn', N'test', 1, N'9999999999', 1, N'hola', N'img/abc@fpt.edu.vn.png')
INSERT [dbo].[User] ([id], [email], [name], [gender], [phone], [status], [address], [avatar]) VALUES (10, N'admin', N'Admin', 1, N'####', 1, N'Admin', NULL)
INSERT [dbo].[User] ([id], [email], [name], [gender], [phone], [status], [address], [avatar]) VALUES (11, N'subaccfordownloadinggames@gmail.com', N'test', 1, N'9999999999', 0, N'', N'')
INSERT [dbo].[User] ([id], [email], [name], [gender], [phone], [status], [address], [avatar]) VALUES (12, N'namtmhe160442@fpt.edu.vn', N'Tran Minh Nam', 1, N'0965151202', 0, N'', N'')
SET IDENTITY_INSERT [dbo].[User] OFF
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Role] FOREIGN KEY([roleId])
REFERENCES [dbo].[Role] ([id])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Role]
GO
ALTER TABLE [dbo].[Account_Function]  WITH CHECK ADD  CONSTRAINT [FK_Account_Function_Account1] FOREIGN KEY([email])
REFERENCES [dbo].[Account] ([email])
GO
ALTER TABLE [dbo].[Account_Function] CHECK CONSTRAINT [FK_Account_Function_Account1]
GO
ALTER TABLE [dbo].[Account_Function]  WITH CHECK ADD  CONSTRAINT [FK_Account_Function_Function] FOREIGN KEY([functionId])
REFERENCES [dbo].[Function] ([id])
GO
ALTER TABLE [dbo].[Account_Function] CHECK CONSTRAINT [FK_Account_Function_Function]
GO
ALTER TABLE [dbo].[Account_Key]  WITH CHECK ADD  CONSTRAINT [FK_Account_Key_Account] FOREIGN KEY([email])
REFERENCES [dbo].[Account] ([email])
GO
ALTER TABLE [dbo].[Account_Key] CHECK CONSTRAINT [FK_Account_Key_Account]
GO
ALTER TABLE [dbo].[Answer]  WITH CHECK ADD  CONSTRAINT [FK_Answer_Question] FOREIGN KEY([questionId])
REFERENCES [dbo].[Question] ([id])
GO
ALTER TABLE [dbo].[Answer] CHECK CONSTRAINT [FK_Answer_Question]
GO
ALTER TABLE [dbo].[Answer_History]  WITH CHECK ADD  CONSTRAINT [FK_Answer_History_Quiz_History] FOREIGN KEY([quizHistoryId])
REFERENCES [dbo].[Quiz_History] ([id])
GO
ALTER TABLE [dbo].[Answer_History] CHECK CONSTRAINT [FK_Answer_History_Quiz_History]
GO
ALTER TABLE [dbo].[Answer_History]  WITH CHECK ADD  CONSTRAINT [FK_Quiz_History_Answer] FOREIGN KEY([answerId])
REFERENCES [dbo].[Answer] ([id])
GO
ALTER TABLE [dbo].[Answer_History] CHECK CONSTRAINT [FK_Quiz_History_Answer]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_Blog_Blog_Category] FOREIGN KEY([subCategoryId])
REFERENCES [dbo].[Blog_Sub_Category] ([id])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_Blog_Blog_Category]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_Blog_User] FOREIGN KEY([authorId])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_Blog_User]
GO
ALTER TABLE [dbo].[Blog_Sub_Category]  WITH CHECK ADD  CONSTRAINT [FK_Blog_Sub_Category_Blog_Category] FOREIGN KEY([categoryId])
REFERENCES [dbo].[Blog_Category] ([id])
GO
ALTER TABLE [dbo].[Blog_Sub_Category] CHECK CONSTRAINT [FK_Blog_Sub_Category_Blog_Category]
GO
ALTER TABLE [dbo].[Dimension]  WITH CHECK ADD  CONSTRAINT [FK_Dimension_Subject] FOREIGN KEY([subjectId])
REFERENCES [dbo].[Subject] ([id])
GO
ALTER TABLE [dbo].[Dimension] CHECK CONSTRAINT [FK_Dimension_Subject]
GO
ALTER TABLE [dbo].[Dimension]  WITH CHECK ADD  CONSTRAINT [FK_Dimension_Type] FOREIGN KEY([typeId])
REFERENCES [dbo].[Dimension_Type] ([id])
GO
ALTER TABLE [dbo].[Dimension] CHECK CONSTRAINT [FK_Dimension_Type]
GO
ALTER TABLE [dbo].[Function_Role]  WITH CHECK ADD  CONSTRAINT [FK_Function_Role_Function] FOREIGN KEY([functionId])
REFERENCES [dbo].[Function] ([id])
GO
ALTER TABLE [dbo].[Function_Role] CHECK CONSTRAINT [FK_Function_Role_Function]
GO
ALTER TABLE [dbo].[Function_Role]  WITH CHECK ADD  CONSTRAINT [FK_Function_Role_Role] FOREIGN KEY([roleId])
REFERENCES [dbo].[Role] ([id])
GO
ALTER TABLE [dbo].[Function_Role] CHECK CONSTRAINT [FK_Function_Role_Role]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [FK_Lesson_Lesson_Content] FOREIGN KEY([lessonContentId])
REFERENCES [dbo].[Lesson_Content] ([id])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [FK_Lesson_Lesson_Content]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [FK_Lesson_Lesson_Type] FOREIGN KEY([typeId])
REFERENCES [dbo].[Lesson_Type] ([id])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [FK_Lesson_Lesson_Type]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [FK_Lesson_Quiz] FOREIGN KEY([quizId])
REFERENCES [dbo].[Quiz] ([id])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [FK_Lesson_Quiz]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [FK_Lesson_Subject] FOREIGN KEY([subjectId])
REFERENCES [dbo].[Subject] ([id])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [FK_Lesson_Subject]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [FK_Lesson_Subject_Topic] FOREIGN KEY([subjectTopicId])
REFERENCES [dbo].[Subject_Topic] ([id])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [FK_Lesson_Subject_Topic]
GO
ALTER TABLE [dbo].[Price_Package]  WITH CHECK ADD  CONSTRAINT [FK_PricePackage_Subject] FOREIGN KEY([subjectId])
REFERENCES [dbo].[Subject] ([id])
GO
ALTER TABLE [dbo].[Price_Package] CHECK CONSTRAINT [FK_PricePackage_Subject]
GO
ALTER TABLE [dbo].[Price_Package_Lesson]  WITH CHECK ADD  CONSTRAINT [FK_PricePackage_Lesson_Lesson] FOREIGN KEY([lessonId])
REFERENCES [dbo].[Lesson] ([id])
GO
ALTER TABLE [dbo].[Price_Package_Lesson] CHECK CONSTRAINT [FK_PricePackage_Lesson_Lesson]
GO
ALTER TABLE [dbo].[Price_Package_Lesson]  WITH CHECK ADD  CONSTRAINT [FK_PricePackage_Lesson_PricePackage] FOREIGN KEY([pricePackageId])
REFERENCES [dbo].[Price_Package] ([id])
GO
ALTER TABLE [dbo].[Price_Package_Lesson] CHECK CONSTRAINT [FK_PricePackage_Lesson_PricePackage]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Dimension] FOREIGN KEY([dimensionId])
REFERENCES [dbo].[Dimension] ([id])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Dimension]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Lesson] FOREIGN KEY([lessonId])
REFERENCES [dbo].[Lesson] ([id])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Lesson]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Level] FOREIGN KEY([levelId])
REFERENCES [dbo].[Level] ([id])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Level]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Media_Type] FOREIGN KEY([mediaTypeId])
REFERENCES [dbo].[Media_Type] ([id])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Media_Type]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Subject] FOREIGN KEY([subjectId])
REFERENCES [dbo].[Subject] ([id])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Subject]
GO
ALTER TABLE [dbo].[Question_History]  WITH CHECK ADD  CONSTRAINT [FK_Question_History_Question] FOREIGN KEY([questionId])
REFERENCES [dbo].[Question] ([id])
GO
ALTER TABLE [dbo].[Question_History] CHECK CONSTRAINT [FK_Question_History_Question]
GO
ALTER TABLE [dbo].[Question_History]  WITH CHECK ADD  CONSTRAINT [FK_Question_History_Quiz_History] FOREIGN KEY([quizHistoryId])
REFERENCES [dbo].[Quiz_History] ([id])
GO
ALTER TABLE [dbo].[Question_History] CHECK CONSTRAINT [FK_Question_History_Quiz_History]
GO
ALTER TABLE [dbo].[Question_Quiz]  WITH CHECK ADD  CONSTRAINT [FK_Question_Quiz_Question] FOREIGN KEY([questionId])
REFERENCES [dbo].[Question] ([id])
GO
ALTER TABLE [dbo].[Question_Quiz] CHECK CONSTRAINT [FK_Question_Quiz_Question]
GO
ALTER TABLE [dbo].[Question_Quiz]  WITH CHECK ADD  CONSTRAINT [FK_Question_Quiz_Quiz] FOREIGN KEY([quizId])
REFERENCES [dbo].[Quiz] ([id])
GO
ALTER TABLE [dbo].[Question_Quiz] CHECK CONSTRAINT [FK_Question_Quiz_Quiz]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [FK_Quiz_Level] FOREIGN KEY([levelId])
REFERENCES [dbo].[Level] ([id])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [FK_Quiz_Level]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [FK_Quiz_Quiz_Type] FOREIGN KEY([typeId])
REFERENCES [dbo].[Quiz_Type] ([id])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [FK_Quiz_Quiz_Type]
GO
ALTER TABLE [dbo].[Quiz_History]  WITH CHECK ADD  CONSTRAINT [FK_Quiz_History_Quiz1] FOREIGN KEY([quizId])
REFERENCES [dbo].[Quiz] ([id])
GO
ALTER TABLE [dbo].[Quiz_History] CHECK CONSTRAINT [FK_Quiz_History_Quiz1]
GO
ALTER TABLE [dbo].[Quiz_History]  WITH CHECK ADD  CONSTRAINT [FK_Quiz_History_Subject] FOREIGN KEY([subjectId])
REFERENCES [dbo].[Subject] ([id])
GO
ALTER TABLE [dbo].[Quiz_History] CHECK CONSTRAINT [FK_Quiz_History_Subject]
GO
ALTER TABLE [dbo].[Quiz_History]  WITH CHECK ADD  CONSTRAINT [FK_Quiz_History_User1] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[Quiz_History] CHECK CONSTRAINT [FK_Quiz_History_User1]
GO
ALTER TABLE [dbo].[Registration]  WITH CHECK ADD  CONSTRAINT [FK_Registration_Price_Package] FOREIGN KEY([packageId])
REFERENCES [dbo].[Price_Package] ([id])
GO
ALTER TABLE [dbo].[Registration] CHECK CONSTRAINT [FK_Registration_Price_Package]
GO
ALTER TABLE [dbo].[Registration]  WITH CHECK ADD  CONSTRAINT [FK_Registration_Subject] FOREIGN KEY([subjectId])
REFERENCES [dbo].[Subject] ([id])
GO
ALTER TABLE [dbo].[Registration] CHECK CONSTRAINT [FK_Registration_Subject]
GO
ALTER TABLE [dbo].[Registration]  WITH CHECK ADD  CONSTRAINT [FK_Registration_User] FOREIGN KEY([lastUpdatedBy])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[Registration] CHECK CONSTRAINT [FK_Registration_User]
GO
ALTER TABLE [dbo].[Subject]  WITH CHECK ADD  CONSTRAINT [FK_Subject_Subject_Category] FOREIGN KEY([subCategoryId])
REFERENCES [dbo].[Subject_Sub_Category] ([id])
GO
ALTER TABLE [dbo].[Subject] CHECK CONSTRAINT [FK_Subject_Subject_Category]
GO
ALTER TABLE [dbo].[Subject_Sub_Category]  WITH CHECK ADD  CONSTRAINT [FK_Subject_Sub_Category_Subject_Category] FOREIGN KEY([categoryId])
REFERENCES [dbo].[Subject_Category] ([id])
GO
ALTER TABLE [dbo].[Subject_Sub_Category] CHECK CONSTRAINT [FK_Subject_Sub_Category_Subject_Category]
GO
ALTER TABLE [dbo].[Temp_Registration]  WITH CHECK ADD  CONSTRAINT [FK_Temp_Registration_Registration] FOREIGN KEY([registrationId])
REFERENCES [dbo].[Registration] ([id])
GO
ALTER TABLE [dbo].[Temp_Registration] CHECK CONSTRAINT [FK_Temp_Registration_Registration]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Account1] FOREIGN KEY([email])
REFERENCES [dbo].[Account] ([email])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Account1]
GO
ALTER TABLE [dbo].[User_Subject]  WITH CHECK ADD  CONSTRAINT [FK_User_Subject_Subject] FOREIGN KEY([subjectId])
REFERENCES [dbo].[Subject] ([id])
GO
ALTER TABLE [dbo].[User_Subject] CHECK CONSTRAINT [FK_User_Subject_Subject]
GO
ALTER TABLE [dbo].[User_Subject]  WITH CHECK ADD  CONSTRAINT [FK_User_Subject_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[User_Subject] CHECK CONSTRAINT [FK_User_Subject_User]
GO
USE [master]
GO
ALTER DATABASE [QuizPracticingSystem] SET  READ_WRITE 
GO
